package com.agero.ncc.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.agero.ncc.R;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.model.Equipment;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Adapter for Equipment Selection.
 */
public class EquipmentSelectionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_FOOTER = 1;
    private static final int TYPE_ITEM = 2;
    private ArrayList<Equipment> mEquipmentResults;
    private boolean[] checkedPosition;
    private Context mContext;
    private OnRecyclerItemClick onRecyclerItemClick;
    private int unAvailableEquipmentPosition = -1;
    private String equipmentId;

    public EquipmentSelectionAdapter(Context context, ArrayList<Equipment> equipmentResults, String equipmentId, OnRecyclerItemClick onRecyclerItemClick) {
        this.equipmentId = equipmentId;
        this.mContext = context;
        this.mEquipmentResults = equipmentResults;
        checkedPosition = new boolean[mEquipmentResults.size()];
        this.onRecyclerItemClick = onRecyclerItemClick;
        setUnAvailableSectionPosition();
    }

    private void setUnAvailableSectionPosition() {
        for (int i = 0; i < mEquipmentResults.size(); i++) {
            checkedPosition[i] = false;
            if (!equipmentId.equalsIgnoreCase("-1") && equipmentId.equalsIgnoreCase(mEquipmentResults.get(i).getEquipmentId())) {
                equipmentId = "-1";
                checkedPosition[i] = true;
            }
            if (mEquipmentResults.get(i) == null || !mEquipmentResults.get(i).getIsAvailable()) {
                unAvailableEquipmentPosition = i;
                return;
            }
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_FOOTER) {
            return new FooterViewHolder(LayoutInflater.from(mContext).inflate(R.layout.equipment_footer_view, parent, false));
        } else {
            return new ItemViewHolder(LayoutInflater.from(mContext).inflate(R.layout.equipment_list_item, parent, false));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == mEquipmentResults.size()) {
            return TYPE_FOOTER;
        }
        return TYPE_ITEM;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof FooterViewHolder) {
            FooterViewHolder footerViewHolder = (FooterViewHolder) holder;
        } else if (holder instanceof ItemViewHolder) {
            ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
//            if (itemViewHolder.getAdapterPosition() == 0 && mEquipmentResults.get(itemViewHolder.getAdapterPosition()).getStatus().equals("Available")) {
//                itemViewHolder.mTextEquipmentSection.setText(mContext.getString(R.string.label_available));
//                itemViewHolder.mTextEquipmentSection.setVisibility(View.VISIBLE);
//            } else if (unAvailableEquipmentPosition == holder.getAdapterPosition()) {
//                itemViewHolder.mTextEquipmentSection.setText(mContext.getString(R.string.label_un_available));
//                itemViewHolder.mTextEquipmentSection.setVisibility(View.VISIBLE);
//            }
            if (mEquipmentResults.size() == position + 1) {
                itemViewHolder.mViewBorderEquipmentList.setVisibility(View.GONE);
            } else {
                itemViewHolder.mViewBorderEquipmentList.setVisibility(View.VISIBLE);
            }
            itemViewHolder.mTextEquipmentType.setTextColor(ContextCompat.getColor(mContext, R.color.ncc_black));
            itemViewHolder.mTextEquipmentPlate.setTextColor(ContextCompat.getColor(mContext, R.color.jobdetail_heading_color));


            if (mEquipmentResults.get(itemViewHolder.getAdapterPosition()) != null) {
                if (mEquipmentResults.get(itemViewHolder.getAdapterPosition()).getIsAvailable()) {
                    if (checkedPosition[position]) {
                        itemViewHolder.mRadioButtonEquipment.setChecked(true);
                    }else{
                        itemViewHolder.mRadioButtonEquipment.setChecked(false);
                    }
                    itemViewHolder.setClickListener();
                    itemViewHolder.mRadioButtonEquipment.setVisibility(View.VISIBLE);
                    itemViewHolder.mImageLock.setVisibility(View.GONE);
                } else {
                    itemViewHolder.mRadioButtonEquipment.setChecked(false);
                    itemViewHolder.mTextEquipmentType.setTextColor(ContextCompat.getColor(mContext, R.color.ncc_text_disabled_color));
                    itemViewHolder.mTextEquipmentPlate.setTextColor(ContextCompat.getColor(mContext, R.color.ncc_text_disabled_color));
                    itemViewHolder.mRadioButtonEquipment.setVisibility(View.INVISIBLE);
                    itemViewHolder.mImageLock.setVisibility(View.VISIBLE);
                    itemViewHolder.removeClickListener();
                }

                itemViewHolder.mTextEquipmentType.setText(mEquipmentResults.get(itemViewHolder.getAdapterPosition()).getEquipmentType());
                itemViewHolder.mTextEquipmentPlate.setText(mEquipmentResults.get(itemViewHolder.getAdapterPosition()).getPlateState() + "-" + mEquipmentResults.get(itemViewHolder.getAdapterPosition()).getPlate() + " " +
                        mContext.getString(R.string.dot) + " " +
                        mEquipmentResults.get(itemViewHolder.getAdapterPosition()).getName());
            }
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mEquipmentResults.size() + 1;
    }

    /**
     * View Holder for Equipment list.
     */
    class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.text_equipment_type)
        TextView mTextEquipmentType;
        @BindView(R.id.radioButton_equipment)
        RadioButton mRadioButtonEquipment;
        @BindView(R.id.image_lock)
        ImageView mImageLock;
        @BindView(R.id.text_equipment_plate)
        TextView mTextEquipmentPlate;
        @BindView(R.id.view_border_equipment_list)
        View mViewBorderEquipmentList;


        ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void setClickListener() {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((HomeActivity) mContext).showProgress();
                    onRecyclerItemClick.onItemClicked(getLayoutPosition());
                }
            });
        }

        void removeClickListener() {
            itemView.setOnClickListener(null);
        }
    }

    /**
     * View Holder for displaying footer view.
     */
    class FooterViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.text_equipment_footer)
        TextView mTextEquipmentFooter;
        @BindView(R.id.view_border_equipment_footer)
        View mViewBorderEquipmentFooter;

        FooterViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    /**
     * Click listener for equipment selection.
     */
    public interface OnRecyclerItemClick {
        /**
         * Callback for item click in equipment selection.
         *
         * @param position
         */
        void onItemClicked(int position);
    }
}
