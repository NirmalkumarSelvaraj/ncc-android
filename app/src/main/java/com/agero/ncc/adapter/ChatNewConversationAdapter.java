package com.agero.ncc.adapter;


import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.agero.ncc.R;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.model.JobDetail;
import com.agero.ncc.model.Profile;
import com.agero.ncc.utils.UiUtils;

import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class ChatNewConversationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_JOB_LAYOUT = 1;
    private static final int TYPE_MAIN_LAYOUT = 0;
    private Context mContext;
    private ArrayList<Profile> userList;
    private ArrayList<JobDetail> jobList;
    public ChatNewConversationAdapter(Context mContext, ArrayList<Profile> userList,ArrayList<JobDetail> jobList) {
        this.mContext = mContext;
        this.userList = userList;
        this.jobList = jobList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == TYPE_MAIN_LAYOUT) {
            return new ItemViewHolder(LayoutInflater.from(mContext).inflate(R.layout.chat_history_child_item, parent, false));
        } else {
            return new JobViewHolder(LayoutInflater.from(mContext).inflate(R.layout.chat_history_job_child_item, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()){
            case TYPE_MAIN_LAYOUT :
                position = position - jobList.size();
//                String userName = (userList.get(position).getFirstName()).trim() + " " + (userList.get(position).getLastName()).trim();

                String userName = "";
                if(!TextUtils.isEmpty(userList.get(position).getFirstName())){
                    userName = userList.get(position).getFirstName().trim();
                }
                if(!TextUtils.isEmpty(userList.get(position).getLastName())){
                    if(userName.length() > 0){
                        userName += " ";
                    }
                    userName += userList.get(position).getLastName().trim();
                }

                if(TextUtils.isEmpty(userName)){
                    userName = "Thread";
                }
                String flName[] = userName.split(" ");
                ItemViewHolder itemViewHolder = (ItemViewHolder)holder;

                if(position == 0){
                    itemViewHolder.mTextUserTitle.setVisibility(View.VISIBLE);
                        if(((HomeActivity)mContext).isUserDispatcherAndDriver() ||((HomeActivity)mContext).isLoggedInDriver()){
                            itemViewHolder.mTextUserTitle.setText(mContext.getResources().getString(R.string.dispatchers));
                        } else {
                            itemViewHolder.mTextUserTitle.setText(mContext.getResources().getString(R.string.team));                        }
                }else {
                    itemViewHolder.mTextUserTitle.setVisibility(View.GONE);
                }

                itemViewHolder.mTextName.setText(userName);
                if (flName.length > 1) {
                    itemViewHolder.mImageProfileTv.setText(("" + flName[0].charAt(0) + flName[1].charAt(0)).toUpperCase(Locale.ENGLISH));
                } else {
                    itemViewHolder.mImageProfileTv.setText(("" + flName[0].charAt(0)).toUpperCase(Locale.ENGLISH));

                }
                break;
            case TYPE_JOB_LAYOUT :

                JobViewHolder feedViewHolder = (JobViewHolder)holder;
                if(position == 0){
                    feedViewHolder.mTextJobTitle.setVisibility(View.VISIBLE);
                }else {
                    feedViewHolder.mTextJobTitle.setVisibility(View.GONE);
                }
                feedViewHolder.mTextJobId.setText(mContext.getResources().getString(R.string.title_job_id )+ UiUtils.getJobIdDisplayFormat(jobList.get(position).getDispatchId()));
                break;
        }
    }




    @Override
    public int getItemViewType(int position) {
        if (isAdPosition(position)) {
            return TYPE_JOB_LAYOUT;
        } else {
            return TYPE_MAIN_LAYOUT;
        }

    }

    private boolean isAdPosition(int position) {
        return position < jobList.size();
    }

    @Override
    public int getItemCount() {
        return userList.size() + jobList.size();
    }

    public void filterList(ArrayList<Profile> filterdNames,ArrayList<JobDetail> jobList) {
        this.userList = filterdNames;
        this.jobList = jobList;
        notifyDataSetChanged();
    }

    static class ItemViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_chat_title_users)
        TextView mTextUserTitle;
        @BindView(R.id.user_profile_image)
        CircleImageView mUserProfileImage;
        @BindView(R.id.image_profile_tv)
        TextView mImageProfileTv;
        @BindView(R.id.text_name)
        TextView mTextName;
        @BindView(R.id.view_border_contact_details)
        View mViewBorderContactDetails;
        @BindView(R.id.constraint_chat_users)
        ConstraintLayout mConstraintChatUsers;

        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    static class JobViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.text_chat_title_jobs)
        TextView mTextJobTitle;
        @BindView(R.id.image_chat_job)
        ImageView mImageJob;
        @BindView(R.id.text_jobId)
        TextView mTextJobId;
        @BindView(R.id.view_border_job_id)
        View mViewBorderJobs;
        public JobViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
