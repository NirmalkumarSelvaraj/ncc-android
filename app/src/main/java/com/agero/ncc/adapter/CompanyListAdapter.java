package com.agero.ncc.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.agero.ncc.R;

import java.security.AccessControlContext;
import java.util.ArrayList;


public class CompanyListAdapter extends RecyclerView.Adapter<CompanyListAdapter.MyHolder> {
    private ArrayList<String> companyList;
    private Context context;
    public OnRecyclerItemClick onRecyclerItemClick;

    public CompanyListAdapter(Context context, ArrayList<String> companyList,OnRecyclerItemClick onRecyclerItemClick) {
        this.context = context;
        this.companyList = companyList;
        this.onRecyclerItemClick = onRecyclerItemClick;

    }

    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.company_list_item,parent,false);
        return new MyHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyHolder holder, int position) {
        holder.textCompanyName.setText(companyList.get(position));
        holder.setClickListener();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return companyList.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder{
        RadioButton radioCompany;
        TextView textCompanyName;
        public MyHolder(View itemView) {
            super(itemView);
            radioCompany = (RadioButton)itemView.findViewById(R.id.radio_button_company);
            textCompanyName = (TextView)itemView.findViewById(R.id.text_company_name);

        }

        void setClickListener() {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onRecyclerItemClick.onItemClicked(getLayoutPosition());
                }
            });
        }
    }
    public interface OnRecyclerItemClick {
        /**
         * Callback for item click in company selection.
         *
         * @param position
         */
        void onItemClicked(int position);
    }

}
