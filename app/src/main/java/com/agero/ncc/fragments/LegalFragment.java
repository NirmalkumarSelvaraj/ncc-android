package com.agero.ncc.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.agero.ncc.BuildConfig;
import com.agero.ncc.R;
import com.agero.ncc.activities.BaseActivity;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.app.NCCApplication;
import com.agero.ncc.model.TermsAndConditionsModel;
import com.agero.ncc.retrofit.NccApi;
import com.agero.ncc.utils.NccConstants;
import com.agero.ncc.utils.UserError;
import com.agero.ncc.utils.Utils;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LegalFragment extends BaseFragment {
    HomeActivity mHomeActivity;
    @BindView(R.id.image_account_back_arrow)
    ImageView mImageAccountBackArrow;
    @BindView(R.id.button_agree)
    Button mButtonAgree;
    @BindView(R.id.webview_legal)
    WebView legalWebview;
    @Inject
    NccApi nccApi;
    private UserError mUserError;

    //    String legal = ""
    public LegalFragment() {
        // Intentionally empty
    }

    public static LegalFragment newInstance() {
        LegalFragment fragment = new LegalFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View superView = super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup fragment_content = superView.findViewById(R.id.fragment_content);

        View view = inflater.inflate(R.layout.fragment_terms_conditions, fragment_content, true);
        ButterKnife.bind(this, view);
        NCCApplication.getContext().getComponent().inject(this);
        mImageAccountBackArrow.setVisibility(View.GONE);
        mButtonAgree.setVisibility(View.GONE);
        mEditor = mPrefs.edit();
        mUserError = new UserError();
        mHomeActivity = (HomeActivity) getActivity();
        if(getResources().getBoolean(R.bool.isTablet)){
            mHomeActivity.showBottomBar();
        } else {
            mHomeActivity.hideBottomBar();
        }
        mHomeActivity.showToolbar(getString(R.string.title_legal));
        getContent();
        return superView;
    }

    private void getContent() {
        if (Utils.isNetworkAvailable()) {
            showProgress();
            nccApi.getTermsAndConditions(BuildConfig.APIGEE_URL + "/profile/terms-and-conditions/terms-conditions", "Bearer "+mPrefs.getString(NccConstants.APIGEE_TOKEN,"")).enqueue(new Callback<TermsAndConditionsModel>() {
                @Override
                public void onResponse(Call<TermsAndConditionsModel> call, Response<TermsAndConditionsModel> response) {
                    if (isAdded()) {
                        if (response != null && response.body() != null) {
                            hideProgress();
                            TermsAndConditionsModel responseModel = response.body();
//                            mTermsContent.setText(responseModel.getContent());

                            if (responseModel != null) {
                                HashMap<String, String> extraDatas = new HashMap<>();
                                extraDatas.put("json", new Gson().toJson(responseModel));
                                mHomeActivity.mintlogEventExtraData("Legal Screen Terms and Conditions", extraDatas);
                            }

                            if(responseModel != null && responseModel.getContent() != null) {
                                String htmlAsString = responseModel.getContent();      // used by WebView
                                Spanned htmlAsSpanned = Html.fromHtml(htmlAsString); // used by TextView
                                legalWebview.loadDataWithBaseURL(null, htmlAsString, "text/html", "UTF-8", null);
                            }
                        } else if (response.errorBody() != null) {
                            hideProgress();
                            try {
                                JSONObject errorResponse = new JSONObject(response.errorBody().string());
                                if (errorResponse != null) {
                                    if (mHomeActivity != null && !mHomeActivity.isFinishing() && errorResponse.get("message") != null && errorResponse.get("message").toString().length() > 1) {
                                        Toast.makeText(mHomeActivity, (String) errorResponse.get("message"), Toast.LENGTH_LONG).show();
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<TermsAndConditionsModel> call, Throwable t) {
                    hideProgress();
                    if(isAdded()) {
                        mHomeActivity.mintlogEvent("Terms and Conditions Api Error - " + t.getLocalizedMessage());
                        Toast.makeText(mHomeActivity, getString(R.string.auth_failed), Toast.LENGTH_LONG).show();
                    }
                }
            });
        } else {
            mUserError.title = ""; mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }
    }
}
