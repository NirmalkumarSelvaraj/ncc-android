package com.agero.ncc.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.agero.ncc.R;
import com.agero.ncc.activities.HomeActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class JobCompleteNotesFragment extends BaseFragment {

    HomeActivity mHomeActivity;
    Unbinder unbinder;

    public JobCompleteNotesFragment() {
        // Intentionally empty
    }

    public static JobCompleteNotesFragment newInstance() {
        JobCompleteNotesFragment fragment = new JobCompleteNotesFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View superView = super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup fragment_content = superView.findViewById(R.id.fragment_content);

        View view = inflater.inflate(R.layout.fragment_jobcomplete_notes, fragment_content, true);
        mHomeActivity = (HomeActivity) getActivity();
        mHomeActivity.hideToolBar();
        unbinder = ButterKnife.bind(this, view);
        return superView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.image_back_arrow, R.id.button_done})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_back_arrow:
                getActivity().onBackPressed();
                break;
            case R.id.button_done:
                mHomeActivity.popAllBackstack();
                mHomeActivity.mBottomBar.setCurrentItem(0);
                break;
        }
    }
}
