package com.agero.ncc.fragments;


import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.widget.Toast;

import com.agero.ncc.R;
import com.agero.ncc.activities.BaseActivity;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.app.NCCApplication;
import com.agero.ncc.firebasefunctions.FirebaseFunctions;
import com.agero.ncc.firebasefunctions.FirebaseFunctionsException;
import com.agero.ncc.firebasefunctions.HttpsCallableResult;
import com.agero.ncc.model.AssignmentModel;
import com.agero.ncc.model.JobDetail;
import com.agero.ncc.model.Status;
import com.agero.ncc.model.StatusUpdateModel;
import com.agero.ncc.utils.DateTimeUtils;
import com.agero.ncc.utils.NccConstants;
import com.agero.ncc.utils.TokenManager;
import com.agero.ncc.utils.UserError;
import com.agero.ncc.utils.Utils;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import durdinapps.rxfirebase2.RxFirebaseDatabase;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import timber.log.Timber;

import static com.agero.ncc.utils.NccConstants.JOB_STATUS_CANCELLED;
import static com.agero.ncc.utils.NccConstants.JOB_STATUS_GOA;

public class BaseStatusFragment extends BaseFragment {

    protected interface StatusUpdateListener {
        void onSuccess();

        void onFailure();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    protected void assignmentAPI(String mDispatchNumber, String mDispatchAssignId, String mDispatchAssignName, String mDriverAssignedById, String mDriverAssignedByName, String previousDispatchAssignedToId, ArrayList<Status> statusHistory, StatusUpdateListener statusUpdateListener) {

        if (Utils.isNetworkAvailable()) {
            showProgress();
            TokenManager.getInstance().validateToken(((BaseActivity) getActivity()), new TokenManager.TokenReponseListener() {
                @Override
                public void onRefreshSuccess() {
                    if (isAdded()) {

                        HashMap<String, Object> assignmentObj = new HashMap<String, Object>();
                        assignmentObj.put("statusCode", NccConstants.JOB_STATUS_ASSIGNED);
                        assignmentObj.put("serverTimeUtc", DateTimeUtils.getUtcTimeFormat());
                        assignmentObj.put("dispatchId", mDispatchNumber);
                        assignmentObj.put("isPending", false);
                        assignmentObj.put("userName", mPrefs.getString(NccConstants.SIGNIN_USER_NAME, ""));
                        assignmentObj.put("userId", mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""));
                        assignmentObj.put("authToken", mPrefs.getString(NccConstants.APIGEE_TOKEN, ""));
                        assignmentObj.put("source", NccConstants.SOURCE);
                        assignmentObj.put("subSource", NccConstants.SUB_SOURCE);
                        assignmentObj.put("deviceId", Settings.Secure.getString(NCCApplication.getContext().getContentResolver(), Settings.Secure.ANDROID_ID));
                        assignmentObj.put("dispatchAssignedToId", mDispatchAssignId);
                        assignmentObj.put("dispatchAssignedToName", mDispatchAssignName);
                        assignmentObj.put("driverAssignedById", mDriverAssignedById);
                        assignmentObj.put("driverAssignedByName", mDriverAssignedByName);
                        assignmentObj.put("previousDispatchAssignedToId", previousDispatchAssignedToId);
                        assignmentObj.put("statusHistory", new Gson().toJson(statusHistory));


                        Timber.d("assignmentAPI", "AssignmentModel created");

                        HashMap<String, String> extraDatas = new HashMap<>();
                        extraDatas.put("current_status", new Gson().toJson(assignmentObj));
                        extraDatas.put("dispatch_id", mDispatchNumber);
                        ((HomeActivity) getActivity()).mintlogEventExtraData("Assignment", extraDatas);


                        FirebaseFunctions.getInstance()
                                .getHttpsCallable(NccConstants.API_JOB_ASSIGNMENT)
                                .call(assignmentObj)
                                .addOnFailureListener(new OnFailureListener() {

                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        if (isAdded()) {
                                            hideProgress();
                                            Timber.d("failed");
                                            e.printStackTrace();
                                            UserError mUserError = new UserError();
                                            if (e instanceof FirebaseFunctionsException) {
                                                FirebaseFunctionsException ffe = (FirebaseFunctionsException) e;
                                                FirebaseFunctionsException.Code code = ffe.getCode();
                                                Object details = ffe.getDetails();
//                                        Toast.makeText(((BaseActivity) getActivity()), ffe.getMessage(), Toast.LENGTH_LONG).show();
                                                ((HomeActivity) getActivity()).mintlogEvent("Assignment: " + " Firebase Functions Error - Error Message -" + ffe.getMessage());

                                                    mUserError.message = ffe.getMessage();
                                                    try {
                                                        JSONObject errorObj = new JSONObject(ffe.getMessage());
                                                        mUserError.title = errorObj.optString("title");
                                                        mUserError.message = errorObj.optString("body");
                                                        showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);

                                                    } catch (Exception parseException) {
                                                        parseException.printStackTrace();
                                                        showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
                                                    }
                                                    showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);


                                            } else if (e.getMessage() != null) {
                                                if (getString(R.string.firebase_logout_error_msg).equalsIgnoreCase(e.getMessage())) {
                                                    ((HomeActivity) getActivity()).tokenRefreshFailed();
                                                } else {
                                                    mUserError.message = e.getMessage();
                                                    showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
                                                    ((HomeActivity) getActivity()).mintlogEvent("Assignment: " + " Firebase Functions Error - Error Message -" + e.getMessage());
                                                }
                                            } else {
                                                ((HomeActivity) getActivity()).mintlogEvent("Assignment Firebase Functions Error - Error Message -  failed");
                                                mUserError.message = "failed";
                                                showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
                                            }
                                            if (statusUpdateListener != null) {
                                                statusUpdateListener.onFailure();
                                            }
                                        }
                                    }
                                })
                                .addOnSuccessListener(new OnSuccessListener<HttpsCallableResult>() {
                                    @Override
                                    public void onSuccess(HttpsCallableResult httpsCallableResult) {
                                        if (isAdded()) {
                                            hideProgress();
                                            Timber.d("Successful");
                                            if (statusUpdateListener != null) {
                                                statusUpdateListener.onSuccess();
                                            }
                                        }
                                    }
                                });
                    }
                }

                @Override
                public void onRefreshFailure() {
                    if(isAdded()) {
                        ((BaseActivity) getActivity()).tokenRefreshFailed();
                    }
                }
            });
        } else {
            UserError mUserError = new UserError();
            mUserError.title = ""; mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }

    }

    protected void updateStatusAPI(String statusCode, String serverTimeUtc, String mDispatchNumber, ArrayList<Status> statusHistoryList, String mDispatchAssignId, String mDispatchAssignName, String mDriverAssignedById, String mDriverAssignedByName, StatusUpdateListener statusUpdateListener) {
        if (Utils.isNetworkAvailable()) {
            showProgress();
            TokenManager.getInstance().validateToken(((BaseActivity) getActivity()), new TokenManager.TokenReponseListener() {
                @Override
                public void onRefreshSuccess() {
                    if (isAdded()) {

                        HashMap<String, Object> statusupdateObj = new HashMap<String, Object>();

                        statusupdateObj.put("serverTimeUtc", serverTimeUtc);
                        statusupdateObj.put("statusCode", statusCode);
                        statusupdateObj.put("dispatchId", mDispatchNumber);
                        statusupdateObj.put("isPending", false);
                        statusupdateObj.put("userName", mPrefs.getString(NccConstants.SIGNIN_USER_NAME, ""));
                        statusupdateObj.put("userId", mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""));
                        statusupdateObj.put("authToken", mPrefs.getString(NccConstants.APIGEE_TOKEN, ""));
                        statusupdateObj.put("statusTime", serverTimeUtc);
                        statusupdateObj.put("source", NccConstants.SOURCE);
                        statusupdateObj.put("subSource", NccConstants.SUB_SOURCE);
                        statusupdateObj.put("deviceId", Settings.Secure.getString(NCCApplication.getContext().getContentResolver(), Settings.Secure.ANDROID_ID));
                        statusupdateObj.put("statusHistory", new Gson().toJson(statusHistoryList));
                        statusupdateObj.put("dispatchAssignedToId", mDispatchAssignId);
                        statusupdateObj.put("dispatchAssignedToName", mDispatchAssignName);
                        statusupdateObj.put("driverAssignedById", mDriverAssignedById);
                        statusupdateObj.put("driverAssignedByName", mDriverAssignedByName);
                        if (NCCApplication.mLocationData != null && NCCApplication.mLocationData.getLatitude() != 0.0 && NCCApplication.mLocationData.getLongitude() != 0.0) {
                            statusupdateObj.put("latitude", NCCApplication.mLocationData.getLatitude());
                            statusupdateObj.put("longitude", NCCApplication.mLocationData.getLongitude());
                        }


                        HashMap<String, String> extraDatas = new HashMap<>();
                        extraDatas.put("current_status", new Gson().toJson(statusupdateObj));
                        extraDatas.put("dispatch_id", mDispatchNumber);
                        ((HomeActivity) getActivity()).mintlogEventExtraData("Update Status", extraDatas);

                        FirebaseFunctions.getInstance()
                                .getHttpsCallable(NccConstants.API_JOB_STATUS_UPDATE)
                                .call(statusupdateObj)
                                .addOnFailureListener(new OnFailureListener() {

                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        if (isAdded()) {
                                            hideProgress();
                                            Timber.d("failed");
//                                    e.printStackTrace();
                                            UserError mUserError = new UserError();
                                            if (e instanceof FirebaseFunctionsException) {
                                                FirebaseFunctionsException ffe = (FirebaseFunctionsException) e;
                                                FirebaseFunctionsException.Code code = ffe.getCode();
                                                Object details = ffe.getDetails();
                                                ((HomeActivity) getActivity()).mintlogEvent("updateStatus: " + statusCode + " Firebase Functions Error - Error Message -" + ffe.getMessage());

                                                    mUserError.message = ffe.getMessage();
                                                    try {
                                                        JSONObject errorObj = new JSONObject(ffe.getMessage());
                                                        mUserError.title = errorObj.optString("title");
                                                        mUserError.message = errorObj.optString("body");
                                                        showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);

                                                    } catch (Exception parseException) {
                                                        parseException.printStackTrace();
                                                        showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
                                                    }

                                                    showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);

                                            } else if (e.getMessage() != null) {
                                                if (getString(R.string.firebase_logout_error_msg).equalsIgnoreCase(e.getMessage())) {
                                                    ((HomeActivity) getActivity()).tokenRefreshFailed();
                                                } else {
                                                    mUserError.message = e.getMessage();
                                                    showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
                                                    ((HomeActivity) getActivity()).mintlogEvent("updateStatus: " + statusCode + " Firebase Functions Error - Error Message -" + e.getMessage());
                                                }
                                            } else {
                                                ((HomeActivity) getActivity()).mintlogEvent("updateStatus Firebase Functions Error - Error Message - failed");
                                                mUserError.message = "failed";
                                                showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
                                            }
                                            if (statusUpdateListener != null) {
                                                statusUpdateListener.onFailure();
                                            }
                                        }
                                    }
                                })
                                .addOnSuccessListener(new OnSuccessListener<HttpsCallableResult>() {
                                    @Override
                                    public void onSuccess(HttpsCallableResult httpsCallableResult) {
                                        if (isAdded()) {
                                            hideProgress();
                                            Timber.d("Successful");
                                            if (statusUpdateListener != null) {
                                                statusUpdateListener.onSuccess();
                                            }
                                            if (statusCode.equalsIgnoreCase(NccConstants.JOB_STATUS_JOB_COMPLETED)) {
                                                showJobCompleteDialog();
                                            }
                                        }
                                    }
                                });
                    }
                }

                @Override
                public void onRefreshFailure() {
                    if(isAdded()) {
                        ((BaseActivity) getActivity()).tokenRefreshFailed();
                    }
                }
            });
        } else {
            UserError mUserError = new UserError();
            mUserError.title = ""; mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }
    }

    protected void updateUndoStatusAPI(String statusCode, String serverTimeUtc, String mDispatchNumber, ArrayList<Status> statusHistoryList, String mDispatchAssignId, String mDispatchAssignName, String mDriverAssignedById, String mDriverAssignedByName, String undoStatusCode, StatusUpdateListener statusUpdateListener) {
        if (Utils.isNetworkAvailable()) {
            showProgress();
            TokenManager.getInstance().validateToken(((BaseActivity) getActivity()), new TokenManager.TokenReponseListener() {
                @Override
                public void onRefreshSuccess() {
                    if (isAdded()) {


                        HashMap<String, Object> statusUndoUpdateObj = new HashMap<String, Object>();

                        statusUndoUpdateObj.put("serverTimeUtc", serverTimeUtc);
                        statusUndoUpdateObj.put("statusCode", statusCode);
                        statusUndoUpdateObj.put("dispatchId", mDispatchNumber);
                        statusUndoUpdateObj.put("isPending", false);
                        statusUndoUpdateObj.put("userName", mPrefs.getString(NccConstants.SIGNIN_USER_NAME, ""));
                        statusUndoUpdateObj.put("userId", mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""));
                        statusUndoUpdateObj.put("authToken", mPrefs.getString(NccConstants.APIGEE_TOKEN, ""));
                        statusUndoUpdateObj.put("statusTime", serverTimeUtc);
                        statusUndoUpdateObj.put("source", NccConstants.SOURCE);
                        statusUndoUpdateObj.put("subSource", NccConstants.SUB_SOURCE);
                        statusUndoUpdateObj.put("deviceId", Settings.Secure.getString(NCCApplication.getContext().getContentResolver(), Settings.Secure.ANDROID_ID));
                        statusUndoUpdateObj.put("statusHistory", new Gson().toJson(statusHistoryList));
                        statusUndoUpdateObj.put("dispatchAssignedToId", mDispatchAssignId);
                        statusUndoUpdateObj.put("dispatchAssignedToName", mDispatchAssignName);
                        statusUndoUpdateObj.put("driverAssignedById", mDriverAssignedById);
                        statusUndoUpdateObj.put("driverAssignedByName", mDriverAssignedByName);
                        statusUndoUpdateObj.put("undoStatusCode", undoStatusCode);


                        HashMap<String, String> extraDatas = new HashMap<>();
                        extraDatas.put("current_status", new Gson().toJson(statusUndoUpdateObj));
                        extraDatas.put("dispatch_id", mDispatchNumber);
                        ((HomeActivity) getActivity()).mintlogEventExtraData("Undo Status", extraDatas);

                        FirebaseFunctions.getInstance()
                                .getHttpsCallable(NccConstants.API_JOB_STATUS_UNDO_UPDATE)
                                .call(statusUndoUpdateObj)
                                .addOnFailureListener(new OnFailureListener() {

                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        if (isAdded()) {
                                            hideProgress();
                                            Timber.d("failed");
//                                    e.printStackTrace();
                                            UserError mUserError = new UserError();
                                            if (e instanceof FirebaseFunctionsException) {
                                                FirebaseFunctionsException ffe = (FirebaseFunctionsException) e;
                                                FirebaseFunctionsException.Code code = ffe.getCode();
                                                Object details = ffe.getDetails();
//                                        Toast.makeText(((BaseActivity) getActivity()), ffe.getMessage(), Toast.LENGTH_LONG).show();
                                                ((HomeActivity) getActivity()).mintlogEvent("Undo status: " + statusCode + " Firebase Functions Error - Error Message -" + ffe.getMessage());
                                                    mUserError.message = ffe.getMessage();
                                                    try {
                                                        JSONObject errorObj = new JSONObject(ffe.getMessage());
                                                        mUserError.title = errorObj.optString("title");
                                                        mUserError.message = errorObj.optString("body");
                                                        showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);

                                                    } catch (Exception parseException) {
                                                        parseException.printStackTrace();
                                                        showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
                                                    }
                                                    showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);

                                            } else if (e.getMessage() != null) {
                                                if (getString(R.string.firebase_logout_error_msg).equalsIgnoreCase(e.getMessage())) {
                                                    ((HomeActivity) getActivity()).tokenRefreshFailed();
                                                } else {
                                                    mUserError.message = e.getMessage();
                                                    showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
                                                    ((HomeActivity) getActivity()).mintlogEvent("Undo status: " + statusCode + " Firebase Functions Error - Error Message -" + e.getMessage());
                                                }
                                            } else {
                                                ((HomeActivity) getActivity()).mintlogEvent("Undo status Firebase Functions Error - Error Message - failed");
                                                mUserError.message = "failed";
                                                showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
                                            }
                                            if (statusUpdateListener != null) {
                                                statusUpdateListener.onFailure();
                                            }
                                        }
                                    }
                                })
                                .addOnSuccessListener(new OnSuccessListener<HttpsCallableResult>() {
                                    @Override
                                    public void onSuccess(HttpsCallableResult httpsCallableResult) {
                                        if (isAdded()) {
                                            hideProgress();
                                            Timber.d("Successful");
                                            if (statusUpdateListener != null) {
                                                statusUpdateListener.onSuccess();
                                            }
                                            if (statusCode.equalsIgnoreCase(NccConstants.JOB_STATUS_JOB_COMPLETED)) {
                                                showJobCompleteDialog();
                                            }
                                        }
                                    }
                                });
                    }
                }

                @Override
                public void onRefreshFailure() {
                    if(isAdded()) {
                        ((BaseActivity) getActivity()).tokenRefreshFailed();
                    }
                }
            });
        } else {
            UserError mUserError = new UserError();
            mUserError.title = ""; mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }
    }

    protected void updateCancelStatusAPI(String reasonCode, String serverTimeUtc, String mDispatchNumber, ArrayList<Status> statusHistoryList, String mDispatchAssignId, String mDispatchAssignName, String mDriverAssignedById, String mDriverAssignedByName, StatusUpdateListener statusUpdateListener) {
        if (Utils.isNetworkAvailable()) {
            showProgress();
            TokenManager.getInstance().validateToken(((BaseActivity) getActivity()), new TokenManager.TokenReponseListener() {
                @Override
                public void onRefreshSuccess() {
                    if (isAdded()) {

                        HashMap<String, Object> statusupdateObj = new HashMap<String, Object>();

                        statusupdateObj.put("serverTimeUtc", serverTimeUtc);
                        statusupdateObj.put("statusCode", JOB_STATUS_CANCELLED);
                        statusupdateObj.put("reasonCode", reasonCode);
                        statusupdateObj.put("dispatchId", mDispatchNumber);
                        statusupdateObj.put("isPending", true);
                        statusupdateObj.put("userName", mPrefs.getString(NccConstants.SIGNIN_USER_NAME, ""));
                        statusupdateObj.put("userId", mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""));
                        statusupdateObj.put("authToken", mPrefs.getString(NccConstants.APIGEE_TOKEN, ""));
                        statusupdateObj.put("statusTime", serverTimeUtc);
                        statusupdateObj.put("source", NccConstants.SOURCE);
                        statusupdateObj.put("subSource", NccConstants.SUB_SOURCE);
                        statusupdateObj.put("deviceId", Settings.Secure.getString(NCCApplication.getContext().getContentResolver(), Settings.Secure.ANDROID_ID));
                        statusupdateObj.put("statusHistory", new Gson().toJson(statusHistoryList));
                        statusupdateObj.put("dispatchAssignedToId", mDispatchAssignId);
                        statusupdateObj.put("dispatchAssignedToName", mDispatchAssignName);
                        statusupdateObj.put("driverAssignedById", mDriverAssignedById);
                        statusupdateObj.put("driverAssignedByName", mDriverAssignedByName);
                        if (NCCApplication.mLocationData != null && NCCApplication.mLocationData.getLatitude() != 0.0 && NCCApplication.mLocationData.getLongitude() != 0.0) {
                            statusupdateObj.put("latitude", NCCApplication.mLocationData.getLatitude());
                            statusupdateObj.put("longitude", NCCApplication.mLocationData.getLongitude());
                        }

                        HashMap<String, String> extraDatas = new HashMap<>();
                        extraDatas.put("current_status", new Gson().toJson(statusupdateObj));
                        extraDatas.put("dispatch_id", mDispatchNumber);
                        ((HomeActivity) getActivity()).mintlogEventExtraData("Update Status", extraDatas);

                        FirebaseFunctions.getInstance()
                                .getHttpsCallable(NccConstants.API_JOB_CANCEL)
                                .call(statusupdateObj)
                                .addOnFailureListener(new OnFailureListener() {

                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        if (isAdded()) {
                                            hideProgress();
                                            Timber.d("failed");
//                                    e.printStackTrace();
                                            UserError mUserError = new UserError();
                                            if (e instanceof FirebaseFunctionsException) {
                                                FirebaseFunctionsException ffe = (FirebaseFunctionsException) e;
                                                FirebaseFunctionsException.Code code = ffe.getCode();
                                                Object details = ffe.getDetails();
//                                        Toast.makeText(((BaseActivity) getActivity()), ffe.getMessage(), Toast.LENGTH_LONG).show();
                                                ((HomeActivity) getActivity()).mintlogEvent("updateStatus: GOA Firebase Functions Error - Error Message -" + ffe.getMessage());
                                                    mUserError.message = ffe.getMessage();
                                                    try {
                                                        JSONObject errorObj = new JSONObject(ffe.getMessage());
                                                        mUserError.title = errorObj.optString("title");
                                                        mUserError.message = errorObj.optString("body");
                                                        showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);

                                                    } catch (Exception parseException) {
                                                        parseException.printStackTrace();
                                                        showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
                                                    }
                                                    showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);

                                            } else if (e.getMessage() != null) {
                                                if (getString(R.string.firebase_logout_error_msg).equalsIgnoreCase(e.getMessage())) {
                                                    ((HomeActivity) getActivity()).tokenRefreshFailed();
                                                } else {
                                                    mUserError.message = e.getMessage();
                                                    showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
                                                    ((HomeActivity) getActivity()).mintlogEvent("updateStatus: GOA Firebase Functions Error - Error Message -" + e.getMessage());
                                                }
                                            } else {
                                                ((HomeActivity) getActivity()).mintlogEvent("updateStatus Firebase Functions Error - Error Message - failed");
                                                mUserError.message = "failed";
                                                showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
                                            }
                                            if (statusUpdateListener != null) {
                                                statusUpdateListener.onFailure();
                                            }
                                        }
                                    }
                                })
                                .addOnSuccessListener(new OnSuccessListener<HttpsCallableResult>() {
                                    @Override
                                    public void onSuccess(HttpsCallableResult httpsCallableResult) {
                                        if (isAdded()) {
                                            hideProgress();
                                            Timber.d("Successful");
                                            if (statusUpdateListener != null) {
                                                statusUpdateListener.onSuccess();
                                            }
                                        }
                                    }
                                });
                    }
                }

                @Override
                public void onRefreshFailure() {
                    if(isAdded()) {
                        ((BaseActivity) getActivity()).tokenRefreshFailed();
                    }
                }
            });
        } else {
            UserError mUserError = new UserError();
            mUserError.title = ""; mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }
    }

    protected void updateGOAStatusAPI(String reasonCode, String serverTimeUtc, String mDispatchNumber, ArrayList<Status> statusHistoryList, String mDispatchAssignId, String mDispatchAssignName, String mDriverAssignedById, String mDriverAssignedByName, StatusUpdateListener statusUpdateListener) {
        if (Utils.isNetworkAvailable()) {
            showProgress();
            TokenManager.getInstance().validateToken(((BaseActivity) getActivity()), new TokenManager.TokenReponseListener() {
                @Override
                public void onRefreshSuccess() {
                    if (isAdded()) {

                        HashMap<String, Object> statusupdateObj = new HashMap<String, Object>();

                        statusupdateObj.put("serverTimeUtc", serverTimeUtc);
                        statusupdateObj.put("statusCode", JOB_STATUS_GOA);
                        statusupdateObj.put("reasonCode", reasonCode);
                        statusupdateObj.put("dispatchId", mDispatchNumber);
                        statusupdateObj.put("isPending", true);
                        statusupdateObj.put("userName", mPrefs.getString(NccConstants.SIGNIN_USER_NAME, ""));
                        statusupdateObj.put("userId", mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""));
                        statusupdateObj.put("authToken", mPrefs.getString(NccConstants.APIGEE_TOKEN, ""));
                        statusupdateObj.put("statusTime", serverTimeUtc);
                        statusupdateObj.put("source", NccConstants.SOURCE);
                        statusupdateObj.put("subSource", NccConstants.SUB_SOURCE);
                        statusupdateObj.put("deviceId", Settings.Secure.getString(NCCApplication.getContext().getContentResolver(), Settings.Secure.ANDROID_ID));
                        statusupdateObj.put("statusHistory", new Gson().toJson(statusHistoryList));
                        statusupdateObj.put("dispatchAssignedToId", mDispatchAssignId);
                        statusupdateObj.put("dispatchAssignedToName", mDispatchAssignName);
                        statusupdateObj.put("driverAssignedById", mDriverAssignedById);
                        statusupdateObj.put("driverAssignedByName", mDriverAssignedByName);
                        if (NCCApplication.mLocationData != null && NCCApplication.mLocationData.getLatitude() != 0.0 && NCCApplication.mLocationData.getLongitude() != 0.0) {
                            statusupdateObj.put("latitude", NCCApplication.mLocationData.getLatitude());
                            statusupdateObj.put("longitude", NCCApplication.mLocationData.getLongitude());
                        }


                        HashMap<String, String> extraDatas = new HashMap<>();
                        extraDatas.put("current_status", new Gson().toJson(statusupdateObj));
                        extraDatas.put("dispatch_id", mDispatchNumber);
                        ((HomeActivity) getActivity()).mintlogEventExtraData("Update Status", extraDatas);

                        FirebaseFunctions.getInstance()
                                .getHttpsCallable(NccConstants.API_JOB_STATUS_GOA_UPDATE)
                                .call(statusupdateObj)
                                .addOnFailureListener(new OnFailureListener() {

                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        if (isAdded()) {
                                            hideProgress();
                                            Timber.d("failed");
//                                    e.printStackTrace();
                                            UserError mUserError = new UserError();
                                            if (e instanceof FirebaseFunctionsException) {
                                                FirebaseFunctionsException ffe = (FirebaseFunctionsException) e;
                                                FirebaseFunctionsException.Code code = ffe.getCode();
                                                Object details = ffe.getDetails();
//                                        Toast.makeText(((BaseActivity) getActivity()), ffe.getMessage(), Toast.LENGTH_LONG).show();
                                                ((HomeActivity) getActivity()).mintlogEvent("updateStatus: GOA Firebase Functions Error - Error Message -" + ffe.getMessage());
                                                    mUserError.message = ffe.getMessage();
                                                    try {
                                                        JSONObject errorObj = new JSONObject(ffe.getMessage());
                                                        mUserError.title = errorObj.optString("title");
                                                        mUserError.message = errorObj.optString("body");
                                                        showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);

                                                    } catch (Exception parseException) {
                                                        parseException.printStackTrace();
                                                        showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
                                                    }
                                                    showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);

                                            } else if (e.getMessage() != null) {
                                                if (getString(R.string.firebase_logout_error_msg).equalsIgnoreCase(e.getMessage())) {
                                                    ((HomeActivity) getActivity()).tokenRefreshFailed();
                                                } else {
                                                    mUserError.message = e.getMessage();
                                                    showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
                                                    ((HomeActivity) getActivity()).mintlogEvent("updateStatus: GOA Firebase Functions Error - Error Message -" + e.getMessage());
                                                }
                                            } else {
                                                ((HomeActivity) getActivity()).mintlogEvent("updateStatus Firebase Functions Error - Error Message - failed");
                                                mUserError.message = "failed";
                                                showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
                                            }
                                            if (statusUpdateListener != null) {
                                                statusUpdateListener.onFailure();
                                            }
                                        }
                                    }
                                })
                                .addOnSuccessListener(new OnSuccessListener<HttpsCallableResult>() {
                                    @Override
                                    public void onSuccess(HttpsCallableResult httpsCallableResult) {
                                        if (isAdded()) {
                                            hideProgress();
                                            Timber.d("Successful");
                                            if (statusUpdateListener != null) {
                                                statusUpdateListener.onSuccess();
                                            }
                                        }
                                    }
                                });
                    }
                }

                @Override
                public void onRefreshFailure() {
                    if(isAdded()) {
                        ((BaseActivity) getActivity()).tokenRefreshFailed();
                    }
                }
            });
        } else {
            UserError mUserError = new UserError();
            mUserError.title = ""; mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }
    }


    public void updateAcceptStatus(HomeActivity mHomeActivity, BaseJobDetailsFragment.IAcceptDecline acceptDecline, JobDetail mCurrentJob, String mDispatchNumber, UserError mUserError, int mEtaValue, Status currentStatusForUpdate, String reasonCode) {
        if (Utils.isNetworkAvailable()) {
            showProgress();
            TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
                @Override
                public void onRefreshSuccess() {
                    if (isAdded()) {
                        Map<String, Object> data = new HashMap<>();
                        data.put("serverTimeUtc", DateTimeUtils.getUtcTimeFormat());
                        data.put("statusCode", NccConstants.JOB_STATUS_ACCEPTED);
                        if (!TextUtils.isEmpty(reasonCode)) {
                            data.put("reasonCode", reasonCode);
                        }
                        data.put("offerId", mCurrentJob.getOfferId());
                        ArrayList<Status> statusHistory = mCurrentJob.getStatusHistory();

                        if (statusHistory != null && statusHistory.size() > 0) {
                            statusHistory.add(mCurrentJob.getCurrentStatus());
                        } else {
                            statusHistory = new ArrayList<Status>();
                            statusHistory.add(mCurrentJob.getCurrentStatus());
                        }
                        data.put("statusHistory", new Gson().toJson(statusHistory));
                        data.put("totalEtaInMinutes", mEtaValue);
                        data.put("dispatchId", mCurrentJob.getDispatchId());
                        data.put("isPending", true);
                        data.put("userName", mPrefs.getString(NccConstants.SIGNIN_USER_NAME, ""));
                        data.put("userId", mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""));
                        data.put("authToken", mPrefs.getString(NccConstants.APIGEE_TOKEN, ""));
                        data.put("statusTime", DateTimeUtils.getUtcTimeFormat());
                        data.put("source", NccConstants.SOURCE);
                        data.put("subSource", NccConstants.SUB_SOURCE);
                        data.put("deviceId", Settings.Secure.getString(NCCApplication.getContext().getContentResolver(), Settings.Secure.ANDROID_ID));

                        HashMap<String, String> extraDatas = new HashMap<>();
                        extraDatas.put("current_status", new Gson().toJson(currentStatusForUpdate));
                        extraDatas.put("dispatch_id", mDispatchNumber);
                        mHomeActivity.mintlogEventExtraData("Update Status: " + NccConstants.JOB_STATUS_ACCEPTED, extraDatas);

                        acceptDecline.enableAcceptButton(false);
                        FirebaseFunctions.getInstance()
                                .getHttpsCallable(NccConstants.API_JOB_ACCEPT)
                                .call(data)
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        if (isAdded()) {
                                            hideProgress();
                                            acceptDecline.enableAcceptButton(true);
                                            Timber.d("failed");
                                            e.printStackTrace();
                                            if (e instanceof FirebaseFunctionsException) {
                                                FirebaseFunctionsException ffe = (FirebaseFunctionsException) e;
                                                FirebaseFunctionsException.Code code = ffe.getCode();
                                                Object details = ffe.getDetails();
//                                            Toast.makeText(((BaseActivity) getActivity()), ffe.getMessage(), Toast.LENGTH_LONG).show();
                                                mHomeActivity.mintlogEvent("updateAcceptStatus Firebase Functions Error - Error Message -" + ffe.getMessage());
                                                    mUserError.message = ffe.getMessage();
                                                    try {
                                                        JSONObject errorObj = new JSONObject(ffe.getMessage());
                                                        mUserError.title = errorObj.optString("title");
                                                        mUserError.message = errorObj.optString("body");
                                                        showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError,dialogNavigateListener);

                                                    } catch (Exception parseException) {
                                                        parseException.printStackTrace();
                                                        showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError,dialogNavigateListener);
                                                    }
                                            } else if (e.getMessage() != null) {
                                                if (getString(R.string.firebase_logout_error_msg).equalsIgnoreCase(e.getMessage())) {
                                                    ((HomeActivity) getActivity()).tokenRefreshFailed();
                                                } else {
                                                    mHomeActivity.mintlogEvent("updateAcceptStatus Firebase Functions Error - Error Message -" + e.getMessage());
                                                    mUserError.message = e.getMessage();
                                                    showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
                                                }
                                            } else {
                                                mHomeActivity.mintlogEvent("updateAcceptStatus Firebase Functions Error - Error Message - failed");
                                                mUserError.message = "failed";
                                                showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
                                            }
                                        }
                                    }
                                })
                                .addOnSuccessListener(new OnSuccessListener<HttpsCallableResult>() {
                                    @Override
                                    public void onSuccess(HttpsCallableResult httpsCallableResult) {
                                        hideProgress();
                                        acceptDecline.enableAcceptButton(true);
                                        Timber.d("Successful");
                                        if (isAdded() && !getResources().getBoolean(R.bool.isTablet)) {
                                            mHomeActivity.popAllBackstack();
                                            mHomeActivity.mBottomBar.setCurrentItem(0);
                                        }
                                    }
                                });
                    }
                }

                @Override
                public void onRefreshFailure() {
                    hideProgress();
                    if(isAdded()) {
                        mHomeActivity.tokenRefreshFailed();
                    }
                }
            });
        } else {
            mUserError.title = ""; mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }
    }


    public void updateDeclineStatus(HomeActivity mHomeActivity, BaseJobDetailsFragment.IAcceptDecline mAcceptDecline, JobDetail mCurrentJob, String mDispatchNumber, UserError mUserError, String reasonCode, String reasonText) {

        if (Utils.isNetworkAvailable()) {
            showProgress();
            TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
                @Override
                public void onRefreshSuccess() {
                    if (isAdded()) {
                        Map<String, Object> data = new HashMap<>();
                        data.put("serverTimeUtc", DateTimeUtils.getUtcTimeFormat());
                        data.put("statusCode", NccConstants.JOB_STATUS_DECLINED);
                        data.put("reasonCode", reasonCode);
                        data.put("offerId", mCurrentJob.getOfferId());
                        data.put("reasonText", reasonText);
                        data.put("dispatchId", mCurrentJob.getDispatchId());
                        data.put("isPending", true);
                        data.put("userName", mPrefs.getString(NccConstants.SIGNIN_USER_NAME, ""));
                        data.put("userId", mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""));
                        data.put("authToken", mPrefs.getString(NccConstants.APIGEE_TOKEN, ""));
                        data.put("statusTime", DateTimeUtils.getUtcTimeFormat());
                        data.put("source", NccConstants.SOURCE);
                        data.put("subSource", NccConstants.SUB_SOURCE);
                        data.put("deviceId", Settings.Secure.getString(NCCApplication.getContext().getContentResolver(), Settings.Secure.ANDROID_ID));

                        ArrayList<Status> statusHistory = mCurrentJob.getStatusHistory();

                        if (statusHistory != null && statusHistory.size() > 0) {
                            statusHistory.add(mCurrentJob.getCurrentStatus());
                        } else {
                            statusHistory = new ArrayList<Status>();
                            statusHistory.add(mCurrentJob.getCurrentStatus());
                        }
                        data.put("statusHistory", new Gson().toJson(statusHistory));

                        HashMap<String, String> extraDatas = new HashMap<>();
                        extraDatas.put("current_status", new Gson().toJson(data));
                        extraDatas.put("dispatch_id", mDispatchNumber);
                        mHomeActivity.mintlogEventExtraData("Update Status: " + NccConstants.JOB_STATUS_DECLINED, extraDatas);

                        mAcceptDecline.enableDeclineButton(false);
                        FirebaseFunctions.getInstance()
                                .getHttpsCallable(NccConstants.API_JOB_DECLINE)
                                .call(data)
                                .addOnFailureListener(new OnFailureListener() {

                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        if (isAdded()) {
                                            hideProgress();
                                            mAcceptDecline.enableDeclineButton(true);
                                            Timber.d("failed");
                                            e.printStackTrace();
                                            if (e instanceof FirebaseFunctionsException) {
                                                FirebaseFunctionsException ffe = (FirebaseFunctionsException) e;
                                                FirebaseFunctionsException.Code code = ffe.getCode();
                                                Object details = ffe.getDetails();
//                                            Toast.makeText(((BaseActivity) getActivity()), ffe.getMessage(), Toast.LENGTH_LONG).show();

                                                mHomeActivity.mintlogEvent("updateDeclineStatus Firebase Functions Error - Error Message -" + ffe.getMessage());
                                                mUserError.message = ffe.getMessage();
                                                try {
                                                    JSONObject errorObj = new JSONObject(ffe.getMessage());
                                                    mUserError.title = errorObj.optString("title");
                                                    mUserError.message = errorObj.optString("body");
                                                    showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError,dialogNavigateListener);

                                                } catch (Exception parseException) {
                                                    parseException.printStackTrace();
                                                    showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError,dialogNavigateListener);
                                                }
//                                                showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
                                            } else if (e.getMessage() != null) {
                                                if (getString(R.string.firebase_logout_error_msg).equalsIgnoreCase(e.getMessage())) {
                                                    ((HomeActivity) getActivity()).tokenRefreshFailed();
                                                }else {
                                                    mHomeActivity.mintlogEvent("updateDeclineStatus Firebase Functions Error - Error Message -" + e.getMessage());
                                                    mUserError.message = e.getMessage();
                                                    showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
                                                }
                                            } else {
                                                mHomeActivity.mintlogEvent("updateDeclineStatus Firebase Functions Error - Error Message - failed");
                                                mUserError.message = "failed";
                                                showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
                                            }

                                            /*if (!getResources().getBoolean(R.bool.isTablet)) {
                                                mHomeActivity.popAllBackstack();
                                                mHomeActivity.mBottomBar.setCurrentItem(0);
                                            }*/
                                        }
                                    }
                                })
                                .addOnSuccessListener(new OnSuccessListener<HttpsCallableResult>() {
                                    @Override
                                    public void onSuccess(HttpsCallableResult httpsCallableResult) {
                                        hideProgress();
                                        mAcceptDecline.enableDeclineButton(true);
                                        Timber.d("Successful");
                                        if (isAdded() && !getResources().getBoolean(R.bool.isTablet)) {
                                            mHomeActivity.popAllBackstack();
                                            mHomeActivity.mBottomBar.setCurrentItem(0);
                                        }
                                    }
                                });

                    }
                }

                @Override
                public void onRefreshFailure() {
                    hideProgress();
                    if(isAdded()) {
                        mHomeActivity.tokenRefreshFailed();
                    }
                }
            });
        } else {
            mUserError.title = ""; mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }
    }

    public void showJobCompleteDialog() {
            if (isAdded() && !getActivity().isFinishing()) {
                try {
                    FragmentManager manager = getFragmentManager();
                    CustomDialogFragment mydialog = CustomDialogFragment.newInstance();
                    mydialog.show(manager, "mydialog");
                }catch (java.lang.IllegalStateException e){
                   e.printStackTrace();
                }
            }
    }
}
