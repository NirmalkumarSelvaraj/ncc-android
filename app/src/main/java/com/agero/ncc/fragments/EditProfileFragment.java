package com.agero.ncc.fragments;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.agero.ncc.BuildConfig;
import com.agero.ncc.R;
import com.agero.ncc.activities.BaseActivity;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.app.NCCApplication;
import com.agero.ncc.model.Profile;
import com.agero.ncc.retrofit.NccApi;
import com.agero.ncc.utils.NccConstants;
import com.agero.ncc.utils.TokenManager;
import com.agero.ncc.utils.UserError;
import com.agero.ncc.utils.Utils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.gson.Gson;
import com.vicmikhailau.maskededittext.MaskedFormatter;
import com.vicmikhailau.maskededittext.MaskedWatcher;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import durdinapps.rxfirebase2.RxFirebaseDatabase;
import durdinapps.rxfirebase2.RxFirebaseStorage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class EditProfileFragment extends CameraBaseFragment implements HomeActivity.ToolbarSaveListener {
    public static final int SHOW_BOTTOM_SHEET_CREATE_ACCOUNT = 1;
    public static final int SHOW_BOTTOM_SHEET_BASIC_DETAILS = 2;
    public static final int SHOW_BOTTOM_SHEET_VEHICLE_CONDITION_PHOTO = 3;
    //Temporarly added
    static boolean isPasswordChange;
    HomeActivity mHomeActivity;
    @BindView(R.id.edit_first_name)
    EditText mEditFirstName;
    @BindView(R.id.edit_last_name)
    EditText mEditLastName;
    @BindView(R.id.edit_email)
    EditText mEditEmail;
    @BindView(R.id.edit_mobile)
    EditText mEditMobile;
    Profile mProfile;
    @BindView(R.id.scollView)
    ScrollView mScollView;
    @BindView(R.id.image_button_camera)
    CircleImageView mImageButtonCamera;
    //    @BindView(R.id.edit_password)
//    EditText mEditPassword;
    @BindView(R.id.image_profile_tv)
    TextView mImageProfileTv;
    //    @BindView(R.id.image_edit)
//    ImageView mImageEdit;
    String mFLname = "";
    DatabaseReference myRef;
    UserError mUserError;
    @BindView(R.id.coordinate_edit_profile)
    CoordinatorLayout mCoordinateEditProfile;
    @BindView(R.id.image_button_view)
    CircleImageView mImageButtonView;
    MaskedFormatter formatter = new MaskedFormatter("+1 (###) ###-####");
    @Inject
    NccApi nccApi;
    private Uri mProfilePhotoUri;
    private long oldRetryTime = 0;
    private MaskedWatcher maskedWatcher;
    ValueEventListener valueEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            try {
                myRef.removeEventListener(valueEventListener);
            } catch (Exception e) {
                e.printStackTrace();
            }
            loadEditProfile(dataSnapshot);
            hideProgress();
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            if (isAdded()) {
                hideProgress();
                if (databaseError != null && databaseError.getMessage() != null) {
                    mHomeActivity.mintlogEvent("Edit Profile User Firebase DatabaseError - Error code - " + databaseError.getCode() + " Error Message -" + databaseError.getMessage());
                }
                if (oldRetryTime == 0 || (System.currentTimeMillis() - oldRetryTime) > NccConstants.RETRY_LIMIT_MILLI_SEC) {
                    getUserDataFromFirebase();
                    oldRetryTime = System.currentTimeMillis();
                }
            }
        }
    };
    private boolean isTextChanged;
    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            if (mEditMobile.getText().toString().length() == 17) {
                enableSave();
            } else {
                mHomeActivity.saveDisable();
            }
        }

        @Override
        public void afterTextChanged(Editable editable) {


        }
    };

    public EditProfileFragment() {
        // Intentionally empty
    }

    public static EditProfileFragment newInstance() {
        EditProfileFragment fragment = new EditProfileFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View superView = super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup fragment_content = superView.findViewById(R.id.fragment_content);
        View view = inflater.inflate(R.layout.fragment_edit_profile, fragment_content, true);
        ButterKnife.bind(this, view);
        NCCApplication.getContext().getComponent().inject(this);
        mEditor = mPrefs.edit();
        mHomeActivity = (HomeActivity) getActivity();
        if (getResources().getBoolean(R.bool.isTablet)) {
            mHomeActivity.showBottomBar();
            mScollView.setVerticalScrollBarEnabled(true);
        } else {
            mHomeActivity.hideBottomBar();
            mScollView.setVerticalScrollBarEnabled(false);
        }
        mHomeActivity.showToolbar(getString(R.string.title_edit_profile));
        mHomeActivity.saveDisable();
        mHomeActivity.setOnToolbarSaveListener(this);
        mUserError = new UserError();


        getUserDataFromFirebase();
        if (isPasswordChange) {
            Snackbar.make(mCoordinateEditProfile, "Password Updated", Snackbar.LENGTH_SHORT).show();
            isPasswordChange = false;
        }
        mEditFirstName.addTextChangedListener(textWatcher);
        mEditLastName.addTextChangedListener(textWatcher);
        mEditEmail.addTextChangedListener(textWatcher);
        mEditMobile.addTextChangedListener(textWatcher);
        return superView;
    }

    private void getUserDataFromFirebase() {
        if (Utils.isNetworkAvailable()) {
            if (!getResources().getBoolean(R.bool.isTablet)) {
                showProgress();
            }
            TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
                @Override
                public void onRefreshSuccess() {
                    if (isAdded()) {
                        FirebaseDatabase database = FirebaseDatabase.getInstance();
                        myRef = database.getReference("Users/" + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "")
                                + "/" + mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""));
                       // myRef.keepSynced(true);
                        myRef.addValueEventListener(valueEventListener);
                    }
                }

                @Override
                public void onRefreshFailure() {
                    hideProgress();
                    if(isAdded()) {
                        mHomeActivity.tokenRefreshFailed();
                    }
                }
            });
        } else {
            mUserError.title = ""; mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }
    }

    private void loadEditProfile(DataSnapshot dataSnapshot) {
        if (dataSnapshot.hasChildren()) {
            try {
                mProfile = dataSnapshot.getValue(Profile.class);

                if(mProfile != null) {
                    HashMap<String, String> extraDatas = new HashMap<>();
                    extraDatas.put("json", new Gson().toJson(mProfile));
                    mHomeActivity.mintlogEventExtraData("Edit Profile User Data", extraDatas);
                }

                if (mProfile.getFirstName().length() > 0 && mProfile.getLastName().length() > 0) {
                    mImageButtonView.setVisibility(View.VISIBLE);
                    mFLname = mProfile.getFirstName().charAt(0) + "" + mProfile.getLastName().charAt(0);
                    mImageProfileTv.setText(mFLname.toUpperCase(Locale.ENGLISH));
                }


                mEditFirstName.setText(Utils.toCamelCase(mProfile.getFirstName()));
                mEditLastName.setText(Utils.toCamelCase(mProfile.getLastName()));
                mEditEmail.setText(mProfile.getEmail());
//            mEditPassword.setText(getString(R.string.settings_password));
                maskedWatcher = new MaskedWatcher(formatter, mEditMobile);
                mEditMobile.addTextChangedListener(maskedWatcher);
                String text = mProfile.getMobilePhoneNumber();
                mEditMobile.setText(text);
                mHomeActivity.saveDisable();
            } catch (Exception e) {
                mHomeActivity.mintLogException(e);
            }
        }
    }

    private void enableSave() {
        if (isAdded()) {
            if ((mProfilePhotoUri != null || isTextFieldsChanged()) && !isTextFieldsEmpty() && isValidEmail(mEditEmail.getText())) {
                mHomeActivity.saveEnable();
            } else {
                mHomeActivity.saveDisable();
            }
        }
    }

    @Override
    public void onSave() {
        updateProfile();

    }

    private void updateProfile() {
        if (Utils.isNetworkAvailable()) {
            showProgress();
            TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
                @Override
                public void onRefreshSuccess() {
                    if (isAdded()) {
                        String url = BuildConfig.APIGEE_URL + "/profile/partners/" + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "")
                                + "/users/" + mPrefs.getString(NccConstants.SIGNIN_USER_ID, "");
                        String token = "Bearer " + mPrefs.getString(NccConstants.APIGEE_TOKEN, "");
                        mProfile.setEmail(mEditEmail.getText().toString().trim());
                        mProfile.setLastName(mEditLastName.getText().toString().trim());
                        mProfile.setFirstName(mEditFirstName.getText().toString().trim());


                        if (!TextUtils.isEmpty(mEditMobile.getText().toString().trim())) {
                            String unMaskedString = formatter.formatString(mEditMobile.getText().toString().trim()).getUnMaskedString();
                            mProfile.setMobilePhoneNumber(unMaskedString);
                        }


                        HashMap<String, String> extraDatas = new HashMap<>();
                        extraDatas.put("json", new Gson().toJson(mProfile));
                        mHomeActivity.mintlogEventExtraData("Profile Modification", extraDatas);

                        nccApi.putProfile(url, token, mProfile).enqueue(new Callback<Object>() {
                            @Override
                            public void onResponse(Call<Object> call, Response<Object> response) {
                                if (isAdded()) {
                                    if (response != null && response.body() != null) {
                                        hideProgress();
                                        saveProfile();
                                    } else if (response.errorBody() != null) {
                                        hideProgress();
                                        try {
                                            JSONObject errorResponse = new JSONObject(response.errorBody().string());
                                            if (errorResponse != null) {
                                                if (mHomeActivity != null && !mHomeActivity.isFinishing() && errorResponse.get("message") != null && errorResponse.get("message").toString().length() > 1) {
                                                    Toast.makeText(mHomeActivity, (String) errorResponse.get("message"), Toast.LENGTH_LONG).show();
                                                }
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<Object> call, Throwable t) {
                                hideProgress();
                                if (isAdded()) {
                                    mHomeActivity.mintlogEvent("Edit Profile Api Error - " + t.getLocalizedMessage());
                                    Toast.makeText(mHomeActivity, getString(R.string.auth_failed), Toast.LENGTH_LONG).show();
                                }
                            }
                        });
                    }
                }

                @Override
                public void onRefreshFailure() {
                    hideProgress();
                    if(isAdded()) {
                        mHomeActivity.tokenRefreshFailed();
                    }
                }
            });
        } else {
            mUserError.title = ""; mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }
    }

//    @Override
//    public void onCancel() {
//        closeScreen();
//    }

    private void closeScreen() {
//        mHomeActivity.setOnToolbarSaveCancelListener(null);
        if(!getResources().getBoolean(R.bool.isTablet)) {
            mHomeActivity.onBackPressed();
        }

    }

    private boolean isTextFieldsChanged() {

        boolean isChanged = false;
        if (!TextUtils.isEmpty(mEditMobile.getText().toString().trim()) && mProfile != null) {
            String unMaskedString = formatter.formatString(mEditMobile.getText().toString().trim()).getUnMaskedString();
            if (!mEditFirstName.getText().toString().trim().equalsIgnoreCase(mProfile.getFirstName()) ||
                    !mEditLastName.getText().toString().trim().equalsIgnoreCase(mProfile.getLastName()) ||
                    !mEditEmail.getText().toString().trim().equalsIgnoreCase(mProfile.getEmail()) ||
                    !unMaskedString.equalsIgnoreCase(mProfile.getMobilePhoneNumber())) {
                isChanged = true;
            }
        }
        isTextChanged = isChanged;
        return isTextChanged;
    }

    private boolean isTextFieldsEmpty() {

        if (TextUtils.isEmpty(mEditFirstName.getText().toString()) || TextUtils.isEmpty(mEditLastName.getText().toString())
                || TextUtils.isEmpty(mEditEmail.getText().toString()) || TextUtils.isEmpty(mEditMobile.getText().toString())) {

            return true;
        }
        return false;
    }

    @OnClick({R.id.image_button_camera})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_button_camera:
//                showPhotoSelectionBottomSheet(SHOW_BOTTOM_SHEET_BASIC_DETAILS);
                break;
//            case R.id.edit_password:
//                mHomeActivity.setOnToolbarSaveCancelListener(null);
//                mHomeActivity.push(UpdatedPasswordFragment.newInstance(), getString(R.string.title_updated_password));
//                break;
//            case R.id.image_edit:
//                mHomeActivity.setOnToolbarSaveCancelListener(null);
//                mHomeActivity.push(UpdatedPasswordFragment.newInstance(), getString(R.string.title_updated_password));
//                break;

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE) {
                mProfilePhotoUri = onMultipleSelectFromGalleryResult(data).get(0);
            } else if (requestCode == REQUEST_CAMERA) {
                mProfilePhotoUri = onCaptureImageResult(data);
            }
            if (mProfilePhotoUri != null) {
                mImageButtonView.setVisibility(View.VISIBLE);
                mImageButtonCamera.setImageURI(mProfilePhotoUri);
            } else {
                mImageButtonView.setVisibility(View.GONE);
            }
            enableSave();
        }
    }

    private void updateProfileFields() {
        if (isAdded()) {
            if (myRef != null && mProfile != null) {
//            To update multiple child in Firebase
                Map<String, Object> profileData = new HashMap<>();
                profileData.put("firstName", mEditFirstName.getText().toString());
                profileData.put("lastName", mEditLastName.getText().toString());
                profileData.put("email", mEditEmail.getText().toString());

                if (!TextUtils.isEmpty(mEditMobile.getText().toString().trim())) {
                    String unMaskedPhoneNumber = formatter.formatString(mEditMobile.getText().toString().trim()).getUnMaskedString();
                    profileData.put("mobilePhoneNumber", unMaskedPhoneNumber);
                }

                myRef.updateChildren(profileData).addOnCompleteListener(task -> {
                    if (isAdded()) {
                        mEditor.putString(NccConstants.SIGNIN_USER_NAME,Utils.toCamelCase(mEditFirstName.getText().toString().trim() + " " + mEditLastName.getText().toString().trim()));
                        showMessage(getString(R.string.edit_profile_success));
                        hideProgress();
                        closeScreen();
                    }
                }).addOnFailureListener(e -> {
                    if(isAdded()) {
                        showError(BaseActivity.Companion.getERROR_SHOW_TYPE_TOAST(), new UserError("", "", getString(R.string.edit_profile_error)));
                        hideProgress();
                    }
                });
            } else {
                hideProgress();
            }
        }
    }

    private void saveProfile() {
        if (mProfilePhotoUri != null && !TextUtils.isEmpty(mProfilePhotoUri.getScheme())) {
            FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();

            String filePath = Utils.getFileName(getActivity().getContentResolver(), mProfilePhotoUri);
            if(!TextUtils.isEmpty(filePath) && filePath.contains(".")){
                StorageReference storageReference = firebaseStorage.getReference()
                        .child("Profile/" + mPrefs.getString(NccConstants.SIGNIN_USER_ID, "")
                                + filePath.substring(filePath.lastIndexOf(".")));

                RxFirebaseStorage.putFile(storageReference, mProfilePhotoUri).subscribe(taskSnapshot -> {
                    if (taskSnapshot.getDownloadUrl() != null) {
                        RxFirebaseDatabase.setValue(myRef.child("imageUrl"), taskSnapshot.getDownloadUrl().toString()).subscribe(() -> {
                            mProfilePhotoUri = null;
                            if (isTextChanged) {
                                updateProfileFields();
                            }
                            hideProgress();
                            mHomeActivity.saveDisable();
                            showMessage(getString(R.string.edit_profile_success));
                            closeScreen();
                        });
                    }
                }, throwable -> {
                    hideProgress();
                    if (isAdded()) {
                        mHomeActivity.mintlogEvent("Edit Profile Update Fields Firebase Database Error - " + throwable.getLocalizedMessage());
                        showError(BaseActivity.Companion.getERROR_SHOW_TYPE_TOAST(), new UserError("", "", getString(R.string.edit_profile_error)));
                    }
                });
            }
        } else if (isTextChanged) {
            updateProfileFields();
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        try {
            if (myRef != null) {
                myRef.removeEventListener(valueEventListener);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if (myRef != null) {
                myRef.removeEventListener(valueEventListener);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
