package com.agero.ncc.fragments;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.RadioButton;

import com.agero.ncc.R;
import com.agero.ncc.activities.BaseActivity;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.adapter.AssignDriverListAdapter;
import com.agero.ncc.app.NCCApplication;
import com.agero.ncc.model.JobDetail;
import com.agero.ncc.model.Profile;
import com.agero.ncc.model.Status;
import com.agero.ncc.utils.NccConstants;
import com.agero.ncc.utils.TokenManager;
import com.agero.ncc.utils.UserError;
import com.agero.ncc.utils.Utils;
import com.agero.ncc.views.RecyclerItemClickListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.agero.ncc.utils.NccConstants.USER_STATUS_BLOCKED;
import static com.agero.ncc.utils.NccConstants.USER_STATUS_DELETED;
import static com.agero.ncc.utils.NccConstants.USER_STATUS_INACTIVE;

public class AssignDriverFragment extends BaseStatusFragment implements HomeActivity.ToolbarSaveListener {
    HomeActivity mHomeActivity;
    UserError mUserError;
    @BindView(R.id.recycler_assign_driver)
    RecyclerView mRecyclerAssignDriver;
    AssignDriverListAdapter mAssignDriverListAdapter;
    ArrayList<Profile> mAssignDriverList;
    //    ArrayList<String> mDriverIdList;
    HashMap<String, Integer> mAssignedJobs = new HashMap<>();
    private int mLastClickedEquipment = -1;
    DatabaseReference myRef;
    private DatabaseReference mJobReference, mDriverJobReference;
    private String mDispatcherId;
    JobDetail mCurrentJob;
    private long oldRetryTime = 0;
//    private int serviceUnFinished = 0;

    private boolean mIsUpdateDriver = false;
    private String mSelectedUserId = null;

    public AssignDriverFragment() {
        // Intentionally empty
    }

    public static AssignDriverFragment newInstance(String dispatcherId, JobDetail mCurrentJob) {
        AssignDriverFragment fragment = new AssignDriverFragment();
        Bundle bundle = new Bundle();
        bundle.putString(NccConstants.DISPATCH_REQUEST_NUMBER, dispatcherId);
        bundle.putString(NccConstants.JOB_DETAIL, new Gson().toJson(mCurrentJob));
        fragment.setArguments(bundle);
        return fragment;
    }

    ValueEventListener valueEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            if (!mIsUpdateDriver) {
                hideProgress();
                myRef.removeEventListener(valueEventListener);
                loadDriverList(dataSnapshot);
//                serviceUnFinished--;
//                if (serviceUnFinished <= 0) {

//                }
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            hideProgress();
            if (isAdded()) {
                if (databaseError != null && databaseError.getMessage() != null) {
                    mHomeActivity.mintlogEvent("Assign Driver Users Firebase DatabaseError - Error code - " + databaseError.getCode() + " Error Message -" + databaseError.getMessage());
                }
                if (oldRetryTime == 0 || (System.currentTimeMillis() - oldRetryTime) > NccConstants.RETRY_LIMIT_MILLI_SEC) {
                    getUserDataFromFirebase();
                    oldRetryTime = System.currentTimeMillis();
                }
            }
        }
    };

    ValueEventListener valueEventListenerForJob = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            hideProgress();
            if (!mIsUpdateDriver) {
                myRef.removeEventListener(valueEventListenerForJob);
                loadCurrentStatus(dataSnapshot);
//                serviceUnFinished--;
//                if (serviceUnFinished <= 0) {
//                    hideProgress();
//                }
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            hideProgress();
            if (isAdded()) {
                if (databaseError != null && databaseError.getMessage() != null) {
                    mHomeActivity.mintlogEvent("Assign Driver Job Firebase DatabaseError - Error code - " + databaseError.getCode() + " Error Message -" + databaseError.getMessage());
                }
                if (oldRetryTime == 0 || (System.currentTimeMillis() - oldRetryTime) > NccConstants.RETRY_LIMIT_MILLI_SEC) {
                    getJobDataFromFirebase();
                    oldRetryTime = System.currentTimeMillis();
                }
            }
        }
    };

    ValueEventListener valueEventListenerForJoblist = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            if (!mIsUpdateDriver) {
                hideProgress();
                mDriverJobReference.removeEventListener(valueEventListenerForJoblist);
                mAssignedJobs.clear();
                loadCurrentJoblist(dataSnapshot);
//                serviceUnFinished--;
//                if (serviceUnFinished <= 0) {
//                    hideProgress();
//                }
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            hideProgress();
            if (isAdded()) {
                if (databaseError != null && databaseError.getMessage() != null) {
                    mHomeActivity.mintlogEvent("Assign Driver Job List Firebase DatabaseError - Error code - " + databaseError.getCode() + " Error Message -" + databaseError.getMessage());
                }
                if (oldRetryTime == 0 || (System.currentTimeMillis() - oldRetryTime) > NccConstants.RETRY_LIMIT_MILLI_SEC) {
                    getJobListDataFromFirebase();
                    oldRetryTime = System.currentTimeMillis();
                }
            }
        }
    };


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        View superView = super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup fragment_content = superView.findViewById(R.id.fragment_content);

        View view = inflater.inflate(R.layout.fragment_assign_driver, fragment_content, true);
        NCCApplication.getContext().getComponent().inject(this);
        ButterKnife.bind(this, view);

        if (getArguments() != null) {
            mDispatcherId = getArguments().getString(NccConstants.DISPATCH_REQUEST_NUMBER, "");
            mCurrentJob = new Gson().fromJson(getArguments().getString(NccConstants.JOB_DETAIL), JobDetail.class);
        }
        mHomeActivity = (HomeActivity) getActivity();
        mHomeActivity.hideBottomBar();
        mHomeActivity.showToolbar(getString(R.string.title_assign_driver));
        mHomeActivity.saveDisable();
        mUserError = new UserError();
        mHomeActivity.setOnToolbarSaveListener(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setAutoMeasureEnabled(true);
        mRecyclerAssignDriver.setLayoutManager(linearLayoutManager);
        mAssignDriverList = new ArrayList<>();

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        mJobReference = database.getReference("ActiveJobs/"
                + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "") + "/" + mDispatcherId);
        mJobReference.keepSynced(true);
        myRef = database.getReference("Users/" + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, ""));
        myRef.keepSynced(true);

        mDriverJobReference = database.getReference("ActiveJobs/"
                + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, ""));
        mDriverJobReference.keepSynced(true);

        getUserDataFromFirebase();


        mRecyclerAssignDriver.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                try {
                    if (mLastClickedEquipment >= 0) {
                        ((RadioButton) mRecyclerAssignDriver.getLayoutManager().findViewByPosition(mLastClickedEquipment).findViewById(R.id.radioButton_assign_driver)).setChecked(false);
                    }
                    ((RadioButton) mRecyclerAssignDriver.getLayoutManager().findViewByPosition(position).findViewById(R.id.radioButton_assign_driver)).setChecked(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                mLastClickedEquipment = position;
                if (mAssignDriverList.get(position).getOnDuty() != null && !mAssignDriverList.get(position).getOnDuty()) {
                    mSelectedUserId = mAssignDriverList.get(position).getUserId();
                    dialogOffDuty(mSelectedUserId);
                } else {
                    assignJobToDriver();
                }
            }
        }));
        return superView;

    }

    private void getUserDataFromFirebase() {
        showProgress();
        TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
            @Override
            public void onRefreshSuccess() {
                if (isAdded()) {
//                    serviceUnFinished++;
                    myRef.addListenerForSingleValueEvent(valueEventListener);
                }
            }

            @Override
            public void onRefreshFailure() {
                hideProgress();
                if (isAdded()) {
                    mHomeActivity.tokenRefreshFailed();
                }
            }
        });

    }

    private void getJobDataFromFirebase() {
        if (Utils.isNetworkAvailable()) {
            showProgress();
            TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
                @Override
                public void onRefreshSuccess() {
                    if (isAdded()) {
//                        serviceUnFinished++;
                        mJobReference.addValueEventListener(valueEventListenerForJob);
                    }
                }

                @Override
                public void onRefreshFailure() {
                    hideProgress();
                    if (isAdded()) {
                        mHomeActivity.tokenRefreshFailed();
                    }
                }
            });
        } else {
            mUserError.title = "";
            mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }

    }

    private void getJobListDataFromFirebase() {
        TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
            @Override


            public void onRefreshSuccess() {
                if (isAdded()) {
//                    serviceUnFinished++;
                    mDriverJobReference.addValueEventListener(valueEventListenerForJoblist);
                }
            }

            @Override
            public void onRefreshFailure() {
                hideProgress();
                if (isAdded()) {
                    mHomeActivity.tokenRefreshFailed();
                }
            }
        });
    }


    private void loadDriverList(DataSnapshot dataSnapshot) {
        mAssignDriverList.clear();
        ArrayList<Profile> unMatchedDriverList = new ArrayList<>();
        if (dataSnapshot.hasChildren() && isAdded()) {
            Iterable<DataSnapshot> driverListSnapshot = dataSnapshot.getChildren();
            for (DataSnapshot driverSnapshot : driverListSnapshot) {
                try {
                    Profile mProfile = driverSnapshot.getValue(Profile.class);
                    if (mProfile.getRoles() != null && mProfile.getRoles().contains("ncc_driver")
                            && mProfile.getStatus() != null
                            && !mProfile.getStatus().equalsIgnoreCase(USER_STATUS_INACTIVE)
                            && !mProfile.getStatus().equalsIgnoreCase(USER_STATUS_DELETED)
                            && !mProfile.getStatus().equalsIgnoreCase(USER_STATUS_BLOCKED)) {

                        if(mCurrentJob!= null && mCurrentJob.getEligibilityCheckRequired()){
                            if(NccConstants.BackgroundCheckStatus.VERIFIED.equalsIgnoreCase(mProfile.getBackgroundCheckStatus()) ||
                                    NccConstants.BackgroundCheckStatus.VERIFIED_BY_LEGAL.equalsIgnoreCase(mProfile.getBackgroundCheckStatus()) && mProfile.getOnDuty()){
                                mAssignDriverList.add(mProfile);
                            }else{
                                unMatchedDriverList.add(mProfile);
                            }
                        }else{
                            mAssignDriverList.add(mProfile);
                        }
                        mProfile.setUserId(driverSnapshot.getKey());
                        //mAssignDriverList.add(mProfile);
                    }
                } catch (Exception e) {
                    mHomeActivity.mintLogException(e);
                }
            }

            if(!mAssignDriverList.isEmpty() && mAssignDriverList.size() == 1 &&  mCurrentJob.getDispatchAssignedToId().equalsIgnoreCase(mAssignDriverList.get(0).getUserId())){
                mAssignDriverList.addAll(unMatchedDriverList);
            }


            if(mCurrentJob.getEligibilityCheckRequired() && mAssignDriverList.isEmpty()){
                mAssignDriverList.addAll(unMatchedDriverList);
            }

            HashMap<String, String> extraDatas = new HashMap<>();
            extraDatas.put("json", new Gson().toJson(mAssignDriverList));
            mHomeActivity.mintlogEventExtraData("Assign Driver Screen Users List", extraDatas);

            Collections.sort(mAssignDriverList, new Comparator<Profile>() {
                @Override
                public int compare(Profile p1, Profile p2) {
                    String name1 = p1.getFirstName() + " " + p1.getLastName();
                    String name2 = p2.getFirstName() + " " + p2.getLastName();
                    if (p1.getOnDuty() == p2.getOnDuty()) {
                        return name1.compareToIgnoreCase(name2);
                    } else if (p2.getOnDuty() != null && p2.getOnDuty()) {
                        return 1;
                    } else {
                        return -1;
                    }
                }
            });

            mLastClickedEquipment = getSelectedDriverPosition(mCurrentJob, mAssignDriverList);
            mAssignDriverListAdapter = new AssignDriverListAdapter(getActivity(), mAssignDriverList, mAssignedJobs, mLastClickedEquipment, mCurrentJob);
            mRecyclerAssignDriver.setAdapter(mAssignDriverListAdapter);

            hideProgress();

            if (Utils.isNetworkAvailable()) {
                getJobListDataFromFirebase();
            } else {
                mUserError.title = "";
                mUserError.message = getString(R.string.network_error_message);
                showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
            }
        }

    }

    private void loadCurrentStatus(DataSnapshot dataSnapshot) {
        if (dataSnapshot.hasChildren() && isAdded()) {
            try {
                mCurrentJob = dataSnapshot.getValue(JobDetail.class);

                HashMap<String, String> extraDatas = new HashMap<>();
                extraDatas.put("json", new Gson().toJson(mCurrentJob));
                mHomeActivity.mintlogEventExtraData("Assign Driver Screen Job", extraDatas);

                if (Utils.isNetworkAvailable()) {
                    getUserDataFromFirebase();
//                    getJobListDataFromFirebase();
                } else {
                    mUserError.title = "";
                    mUserError.message = getString(R.string.network_error_message);
                    showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
                }
            } catch (Exception e) {
                mHomeActivity.mintLogException(e);
            }
        }
    }

    private void loadCurrentJoblist(DataSnapshot dataSnapshot) {
        if (dataSnapshot.hasChildren() && isAdded()) {

            for (Profile profile :
                    mAssignDriverList) {
                mAssignedJobs.put(profile.getUserId(), 0);
            }

            Iterable<DataSnapshot> childList = dataSnapshot.getChildren();
            String logEventText = "";
            for (DataSnapshot data : childList) {
                try {
                    JobDetail job = data.getValue(JobDetail.class);
                    if (job != null && job.getDispatchId() != null) {
                        if (!logEventText.isEmpty()) {
                            logEventText += ", ";
                        }
                        logEventText += job.getDispatchId();
                        driverList(job);
                    }
                } catch (Exception e) {
                    mHomeActivity.mintLogException(e);
                }
            }

            mAssignDriverListAdapter.notifyDataSetChanged();

            HashMap<String, String> extraDatas = new HashMap<>();
            extraDatas.put("jobs", logEventText);
            mHomeActivity.mintlogEventExtraData("Assign Driver Screen Job List", extraDatas);
        }
        mAssignDriverListAdapter = new AssignDriverListAdapter(getActivity(), mAssignDriverList, mAssignedJobs, mLastClickedEquipment, mCurrentJob);
        mRecyclerAssignDriver.setAdapter(mAssignDriverListAdapter);
    }

    //Filter the job assigned for indiviual driver
    private void driverList(JobDetail job) {
        if (job != null && job.getDispatchAssignedToId() != null && !job.getDispatchAssignedToId().isEmpty()) {
            if (mAssignedJobs.containsKey(job.getDispatchAssignedToId())) {
                int jobCount = mAssignedJobs.get(job.getDispatchAssignedToId()) + 1;
                mAssignedJobs.put(job.getDispatchAssignedToId(), jobCount);
            } else {
                mAssignedJobs.put(job.getDispatchAssignedToId(), 1);

            }
        }
    }

    @Override
    public void onSave() {
        assignJobToDriver();
    }

    private void assignJobToDriver() {
        mIsUpdateDriver = true;


        ArrayList<Status> statusHistory = mCurrentJob.getStatusHistory();

        if (statusHistory != null && statusHistory.size() > 0) {
            statusHistory.add(mCurrentJob.getCurrentStatus());
        } else {
            statusHistory = new ArrayList<Status>();
            statusHistory.add(mCurrentJob.getCurrentStatus());
        }

        assignmentAPI(mDispatcherId,
                mAssignDriverList.get(mLastClickedEquipment).getUserId(),
                mAssignDriverList.get(mLastClickedEquipment).getFirstName()
                        + " " + mAssignDriverList.get(mLastClickedEquipment).getLastName(), mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""),
                mPrefs.getString(NccConstants.SIGNIN_USER_NAME, ""), mCurrentJob.getDispatchAssignedToId(), statusHistory, new StatusUpdateListener() {
                    @Override
                    public void onSuccess() {
                        mIsUpdateDriver = false;
                        mHomeActivity.popBackStackImmediate();
                    }

                    @Override
                    public void onFailure() {

                    }
                });


    }

    /*private void assignJobToDriver() {
        if (Utils.isNetworkAvailable()) {
            showProgress();
            TokenManager.getInstance().validateToken(((BaseActivity) getActivity()), new TokenManager.TokenReponseListener() {
                @Override
                public void onRefreshSuccess() {
                    if (isAdded()) {
                        mJobReference.runTransaction(new Transaction.Handler() {
                            @Override
                            public Transaction.Result doTransaction(MutableData mutableData) {

                                if (mCurrentJob.getDispatchAssignedToId() == null || mutableData.getValue(JobDetail.class).getDispatchAssignedToId() == null ||
                                        mCurrentJob.getDispatchAssignedToId().equalsIgnoreCase(mutableData.getValue(JobDetail.class).getDispatchAssignedToId())
                                                && !mAssignDriverList.get(mLastClickedEquipment).getUserId().equalsIgnoreCase(mCurrentJob.getDispatchAssignedToId())) {
                                    JobDetail jobDetail = mutableData.getValue(JobDetail.class);

                                    ArrayList<Status> statusHistoryForUpdate = new ArrayList<>();
                                    if (mCurrentJob.getStatusHistory() != null) {
                                        statusHistoryForUpdate = mCurrentJob.getStatusHistory();
                                    }

                                    statusHistoryForUpdate.add(mCurrentJob.getCurrentStatus());


                                    Status currentStatusForUpdate = new Status();
                                    currentStatusForUpdate.setAppId(NccConstants.SUB_SOURCE);
                                    currentStatusForUpdate.setStatusCode(JOB_STATUS_ASSIGNED);
                                    currentStatusForUpdate.setUserId(mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""));
                                    currentStatusForUpdate.setUserName(mPrefs.getString(NccConstants.SIGNIN_USER_NAME, ""));
                                    currentStatusForUpdate.setStatusTime(DateTimeUtils.getUtcTimeFormat());
                                    currentStatusForUpdate.setServerTimeUtc(DateTimeUtils.getUtcTimeFormat());
                                    //currentStatusForUpdate.setSource("FIREBASE");
                                    currentStatusForUpdate.setSubSource("ANDROID");
                                    currentStatusForUpdate.setDeviceId(Settings.Secure.getString(NCCApplication.getContext().getContentResolver(), Settings.Secure.ANDROID_ID));

                                    jobDetail.setStatusHistory(statusHistoryForUpdate);
                                    jobDetail.setCurrentStatus(currentStatusForUpdate);

                                    jobDetail.setDispatchAssignedToName(mAssignDriverList.get(mLastClickedEquipment).getFirstName()
                                            + " " + mAssignDriverList.get(mLastClickedEquipment).getLastName());
                                    jobDetail.setDriverAssignedById(mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""));
                                    jobDetail.setDriverAssignedByName(mPrefs.getString(NccConstants.SIGNIN_USER_NAME, ""));
                                    jobDetail.setDispatchAssignedToId(mAssignDriverList.get(mLastClickedEquipment).getUserId());

                                    mutableData.setValue(jobDetail);

                                    HashMap<String, String> extraDatas = new HashMap<>();
                                    extraDatas.put("job", new Gson().toJson(jobDetail));
                                    mHomeActivity.mintlogEventExtraData("Assign driver job", extraDatas);

                                    return Transaction.success(mutableData);
                                } else {
                                    return Transaction.abort();
                                }

                            }

                            @Override
                            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
                                hideProgress();
                                mIsUpdateDriver = false;
                                mHomeActivity.popBackStackImmediate();


                            }
                        });
                    }
                }

                @Override
                public void onRefreshFailure() {
                    mHomeActivity.tokenRefreshFailed();
                }
            });
        }
    }*/

    int getSelectedDriverPosition(JobDetail
                                          mCurrentJob, ArrayList<Profile> mAssignDriverList) {
        for (int i = 0; i < mAssignDriverList.size(); i++) {
            String mCurrentUsername = mCurrentJob.getDispatchAssignedToId();
            String mDriverUserName = (mAssignDriverList.get(i).getUserId());
            if (mCurrentUsername.equalsIgnoreCase(mDriverUserName)) {
                return i;
            }
        }
        return -1;
    }

    private void dialogOffDuty(String userId) {
        AlertDialogFragment alert = AlertDialogFragment.newDialog(getString(R.string.dialog_off_duty_title), Html.fromHtml("<font color=\"#757575\">" + getString(R.string.dialog_off_duty_message) + "</font>")
                , getString(R.string.dialog_off_duty_continue), getString(R.string.dialog_off_dutty_cancel));
        alert.setListener(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (i == -1) {
                    mHomeActivity.push(EquipmentSelectionFragment.newInstance(mAssignDriverList.get(mLastClickedEquipment).getUserId(), mAssignDriverList.get(mLastClickedEquipment).getFirstName()
                            + " " + mAssignDriverList.get(mLastClickedEquipment).getLastName(), mCurrentJob));
                }
                dialogInterface.dismiss();
            }
        });
        alert.show(getFragmentManager().beginTransaction(), AssignDriverFragment.class.getClass().getCanonicalName());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        removeValueEventListners();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        removeValueEventListners();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        removeValueEventListners();
    }

    private void removeValueEventListners() {
        try {
            if (myRef != null) {
                myRef.removeEventListener(valueEventListener);
            }
            if (mJobReference != null) {
                mJobReference.removeEventListener(valueEventListenerForJob);
            }
            if (mDriverJobReference != null) {
                mDriverJobReference.removeEventListener(valueEventListenerForJoblist);
            }
        } catch (Exception e) {

        }
    }

}
