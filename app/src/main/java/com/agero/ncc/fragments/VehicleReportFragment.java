package com.agero.ncc.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.agero.ncc.R;
import com.agero.ncc.activities.BaseActivity;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.adapter.VehiclePhotoAdapter;
import com.agero.ncc.firestore.FirestoreJobData;
import com.agero.ncc.model.VehicleReportModel;
import com.agero.ncc.model.VehicleReportModelEntity;
import com.agero.ncc.utils.DateTimeUtils;
import com.agero.ncc.utils.FileUtils;
import com.agero.ncc.utils.NccConstants;
import com.agero.ncc.utils.TokenManager;
import com.agero.ncc.utils.UiUtils;
import com.agero.ncc.utils.UserError;
import com.agero.ncc.utils.Utils;
import com.agero.ncc.views.RecyclerItemClickListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.gson.Gson;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import durdinapps.rxfirebase2.RxFirebaseDatabase;
import durdinapps.rxfirebase2.RxFirebaseStorage;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.agero.ncc.utils.FileUtils.FileType.IMAGE;
import static com.agero.ncc.utils.FileUtils.FileType.PDF;
import static com.agero.ncc.utils.FileUtils.FileType.VIDEO;

public class VehicleReportFragment extends CameraBaseFragment
        implements HomeActivity.ToolbarSaveListener {


    public ArrayList<VehicleReportModelEntity> mPhotoUriList = new ArrayList<>();
    public ArrayList<VehicleReportModelEntity> mFirebaseData = new ArrayList<>();
    Unbinder unbinder;
    HomeActivity mHomeActivity;
    VehiclePhotoAdapter mVehiclePhotoAdapter;
    DatabaseReference modelReference;
    @BindView(R.id.recycler_vehicle_condition_photo)
    RecyclerView mRecyclerVehicleConditionPhoto;
    @BindView(R.id.text_description)
    TextView mTextDescription;
    @BindView(R.id.edit_add_notes)
    EditText mEditAddNotes;
    UserError mUserError;
    //    Runtime Permission listener to access external storage.
    PermissionListener externalPermissionListener = new PermissionListener() {
        @Override
        public void onPermissionGranted(PermissionGrantedResponse response) {
        }

        @Override
        public void onPermissionDenied(PermissionDeniedResponse response) {
            if (response.isPermanentlyDenied()) {
                isPermissionDenied = true;
            }
        }

        @Override
        public void onPermissionRationaleShouldBeShown(PermissionRequest permission,
                                                       PermissionToken token) {
            token.continuePermissionRequest();
        }
    };
    private String mDispatcherId;
    private boolean isEditable;
    private int mOldIndex = 0;
    private String mServerNotes = "";
    private long oldRetryTime = 0;
    private int mImageCount = 0, mVideoCount = 0, mPdfCount = 0;
    private int pendingDeleteCount = 0;
    private boolean isFilesRemoved = false;
    private int updateCount = 0, fileCount = 0;

    private CompositeDisposable disposablesCompressor = new CompositeDisposable();
    public VehicleReportFragment() {
        // Intentionally empty
    }

    public static VehicleReportFragment newInstance(String dispatchId, boolean isEditable, boolean isItFromHistory) {
        VehicleReportFragment fragment = new VehicleReportFragment();
        Bundle bundle = new Bundle();
        bundle.putString(NccConstants.DISPATCH_REQUEST_NUMBER, dispatchId);
        bundle.putBoolean(NccConstants.IS_EDITABLE, isEditable);
        bundle.putBoolean(NccConstants.IS_IT_FROM_HISTORY, isItFromHistory);
        fragment.setArguments(bundle);
        return fragment;
    }

    public static VehicleReportFragment newInstance(String dispatchId, boolean isEditable, ArrayList<VehicleReportModelEntity> vehicleReportModelEntities, String notes, ArrayList<VehicleReportModelEntity> firebaseData, String serverNotes, boolean isItFromHistory) {
        VehicleReportFragment fragment = new VehicleReportFragment();
        Bundle bundle = new Bundle();
        bundle.putString(NccConstants.DISPATCH_REQUEST_NUMBER, dispatchId);
        bundle.putBoolean(NccConstants.IS_EDITABLE, isEditable);
        bundle.putBoolean(NccConstants.IS_IT_FROM_HISTORY, isItFromHistory);
        bundle.putString(NccConstants.NOTES, notes);
        bundle.putString(NccConstants.SERVER_DATA_NOTES, serverNotes);
        if (vehicleReportModelEntities != null) {
            bundle.putSerializable(NccConstants.VEHICLE_REPORT_ENTITIES, vehicleReportModelEntities);
        }
        if (firebaseData != null) {
            bundle.putSerializable(NccConstants.SERVER_DATA, firebaseData);
        }
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View superView = super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup fragment_content = superView.findViewById(R.id.fragment_content);


        View view = inflater.inflate(R.layout.fragment_vehicle_report, fragment_content, true);
        unbinder = ButterKnife.bind(this, view);

        if (getArguments() != null) {
            mDispatcherId = getArguments().getString(NccConstants.DISPATCH_REQUEST_NUMBER);
            isEditable = getArguments().getBoolean(NccConstants.IS_EDITABLE);
        }
        mHomeActivity = (HomeActivity) getActivity();
        mUserError = new UserError();
        mTextDescription.setVisibility(View.VISIBLE);
        if (!isEditable) {
            mHomeActivity.showToolbar(getString(R.string.title_vehicle_condition_report_history));

        } else {
            mHomeActivity.showToolbar(getString(R.string.title_vehicle_condition_report));
        }

        mHomeActivity.setOnToolbarSaveListener(this);
        mHomeActivity.saveDisable();
        mTextDescription.setText(Html.fromHtml(getString(R.string.vehicle_report_description)));
        initFirebase();
        mPhotoUriList = new ArrayList<>();
        mFirebaseData.clear();
        addSelectPhoto();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && isEditable) {
            if (!Dexter.isRequestOngoing()) {
                Dexter.checkPermission(externalPermissionListener, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }
        }
         boolean loadFromFirestore = false;
        if (getArguments().containsKey(NccConstants.VEHICLE_REPORT_ENTITIES)) {
            ArrayList<VehicleReportModelEntity> vehicleReportModelEntities = (ArrayList<VehicleReportModelEntity>) getArguments().getSerializable(NccConstants.VEHICLE_REPORT_ENTITIES);
            mFirebaseData = (ArrayList<VehicleReportModelEntity>) getArguments().getSerializable(NccConstants.SERVER_DATA);
            mPhotoUriList.addAll(vehicleReportModelEntities);
            mOldIndex = vehicleReportModelEntities.size();
            mServerNotes = getArguments().getString(NccConstants.SERVER_DATA_NOTES);
            mEditAddNotes.setText(getArguments().getString(NccConstants.NOTES));
            enableDisableSave();
        } else {
            if(FirestoreJobData.getInStance().isFirestore()){
               loadFromFirestore = true;
            }else {
                getDataFromFirebase();
            }
        }


        if (!isEditable) {
            mEditAddNotes.setEnabled(false);
            mHomeActivity.hideSaveButton();
            mTextDescription.setVisibility(View.GONE);
        }

        mRecyclerVehicleConditionPhoto.addOnItemTouchListener(
                new RecyclerItemClickListener(mHomeActivity,
                        new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                if (position == 0) {
                                    if (isEditable && mPhotoUriList.size() < 13) {
                                        showPhotoSelectionBottomSheet(SHOW_BOTTOM_SHEET_VEHICLE_CONDITION_PHOTO);
                                    }
                                } else {
                                    if (isEditable) {
                                        mOldIndex = position - 1;
                                        startTaggingFragment(mPhotoUriList);
                                    }
                                }
                            }
                        }));
        setAdapter();

        setTextWatcherForNotes();

        if(loadFromFirestore){
            updateUiForData(FirestoreJobData.getInStance().getJobDetail().getReportAttachment());
        }

        return superView;
    }

    private void getDataFromFirebase() {
        if (Utils.isNetworkAvailable()) {
            showProgress();
            TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
                @Override
                public void onRefreshSuccess() {
                    if (isAdded()) {
                        RxFirebaseDatabase.observeValueEvent(modelReference, VehicleReportModel.class).subscribe(taskSnapshot -> {
                            hideProgress();
                            updateUiForData(taskSnapshot);
                        }, throwable -> {
                            hideProgress();
                            if (isAdded()) {
                                mHomeActivity.mintlogEvent("Vehicle Report Get Data Firebase Database Error - " + throwable.getLocalizedMessage());
                                if (oldRetryTime == 0 || (System.currentTimeMillis() - oldRetryTime) > NccConstants.RETRY_LIMIT_MILLI_SEC) {
                                    getDataFromFirebase();
                                    oldRetryTime = System.currentTimeMillis();
                                }
                            }
//            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_TOAST(), new UserError("", "", getString(R.string.error_failed_to_upload_documents)));
                        });

                    }
                }

                @Override
                public void onRefreshFailure() {
                    hideProgress();
                    if(isAdded()) {
                        mHomeActivity.tokenRefreshFailed();
                    }
                }
            });
        } else {
            mUserError.title = ""; mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }
    }

    private void updateUiForData(VehicleReportModel taskSnapshot) {
        mPhotoUriList.clear();
        hideProgress();
        addSelectPhoto();
        if (taskSnapshot != null) {
            mFirebaseData.clear();

            if (taskSnapshot != null) {
                HashMap<String, String> extraDatas = new HashMap<>();
                extraDatas.put("dispatch_id", mDispatcherId);
                extraDatas.put("json", new Gson().toJson(taskSnapshot));
                mHomeActivity.mintlogEventExtraData("Vehicle Report Data", extraDatas);
            }

            if (taskSnapshot.getDocuments() != null) {
                for (VehicleReportModelEntity vehicleReportModelEntity1 :
                        taskSnapshot.getDocuments()) {
                    ArrayList<String> tags = new ArrayList<>();
                    tags.add(vehicleReportModelEntity1.getTags().get(0));
                    tags.add(vehicleReportModelEntity1.getTags().get(1));
                    VehicleReportModelEntity cloneObj = new VehicleReportModelEntity(vehicleReportModelEntity1.getType(), vehicleReportModelEntity1.getLocalUri(), vehicleReportModelEntity1.getDocUrl(), vehicleReportModelEntity1.getThumbUrl(), tags, vehicleReportModelEntity1.getVideoLength());
                    mFirebaseData.add(cloneObj);
                }

                mPhotoUriList.addAll(taskSnapshot.getDocuments());
                mOldIndex = mPhotoUriList.size() - 1;
            }
            mServerNotes = taskSnapshot.getNotes();
            if (!isEditable) {
                mPhotoUriList.remove(0);
            }
            mVehiclePhotoAdapter.notifyDataSetChanged();
            if (mServerNotes == null) {
                mServerNotes = "";
            }
            mEditAddNotes.setText(String.valueOf(mServerNotes));

        }
    }

    private void addSelectPhoto() {

        if (!(mPhotoUriList.size() > 0 && mPhotoUriList.get(0).getDocUrl() == null && mPhotoUriList.get(0).getLocalUri() == null)) {
            VehicleReportModelEntity vehicleReportModelEntity = new VehicleReportModelEntity("Select Photo", null, null, null, null, null);
            if (mPhotoUriList.size() > 0) {
                mPhotoUriList.add(0, vehicleReportModelEntity);
            } else {
                mPhotoUriList.add(vehicleReportModelEntity);
            }
        }

    }

    private void initFirebase() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();

        String initUrl = "ActiveJobs/";
        if (getArguments().getBoolean(NccConstants.IS_IT_FROM_HISTORY)) {
            initUrl = "InActiveJobs/";
        }

        modelReference = database.getReference(initUrl + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "")
                + "/" + mDispatcherId + "/reportAttachment");
      //  modelReference.keepSynced(true);

    }

    private void setAdapter() {

        addSelectPhoto();

        if (!isEditable) {
            mPhotoUriList.remove(0);
        }

        mVehiclePhotoAdapter = new VehiclePhotoAdapter(mHomeActivity, mPhotoUriList);
        mRecyclerVehicleConditionPhoto.setAdapter(mVehiclePhotoAdapter);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        //unbinder.unbind();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        ArrayList<VehicleReportModelEntity> list = new ArrayList<>();
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE) {
                if (data.getData() != null && (FileUtils.getFileType(data.getData().getPath()) == PDF)) {
                    VehicleReportModelEntity vehicleReportModelEntity = new VehicleReportModelEntity(null, data.getData(), null, null, null, null);
                    list.add(vehicleReportModelEntity);
                } else {
                    ArrayList<Uri> uris = onMultipleSelectFromGalleryResult(data);
                    ArrayList<VehicleReportModelEntity> vehicleReportModelEntities = new ArrayList<>();
                    VehicleReportModelEntity vehicleReportModelEntity;
                    for (Uri uri :
                            uris) {
                        vehicleReportModelEntity = new VehicleReportModelEntity(null, uri, null, null, null, null);
                        vehicleReportModelEntities.add(vehicleReportModelEntity);
                    }
                    list.addAll(vehicleReportModelEntities);

                }
                if (list.size() > 0) {
                    checkFilesLimit(list);
                }
            } else if (requestCode == REQUEST_CAMERA || requestCode == REQUEST_CAMERA_VIDEO) {
                if (data != null && data.getData() != null && (FileUtils.getFileType(data.getData().getPath()) == VIDEO)) {
                    VehicleReportModelEntity vehicleReportModelEntity = new VehicleReportModelEntity(null, data.getData(), null, null, null, null);
                    list.add(vehicleReportModelEntity);
                } else {
                    if(!TextUtils.isEmpty(mCurrentPhotoPath)) {
                        VehicleReportModelEntity vehicleReportModelEntity = new VehicleReportModelEntity(null, Uri.fromFile(new File(mCurrentPhotoPath)), null, null, null, null);
                        list.add(vehicleReportModelEntity);
                    }
                }

                if (list.size() > 0) {
                    checkFilesLimit(list);

                }
            }
        }

    }

    private void checkFilesLimit(ArrayList<VehicleReportModelEntity> uriList) {
        mPdfCount = 0;
        mImageCount = 0;
        mVideoCount = 0;
        calculateFileCount(mPhotoUriList);
        if (uriList != null && uriList.size() > 0) {
            for (VehicleReportModelEntity vehicleReportModelEntity :
                    uriList) {
                FileUtils.FileType fileType = getFileType(vehicleReportModelEntity);
                if (fileType != null) {
                    switch (fileType) {
                        case PDF:
                            if (mPdfCount == 0) {
                                mPdfCount++;
                                mPhotoUriList.add(vehicleReportModelEntity);
                            }
                            break;
                        case VIDEO:
                            if (mVideoCount == 0) {
                                mVideoCount++;
                                mPhotoUriList.add(vehicleReportModelEntity);
                            }
                            break;
                        case IMAGE:
                            if (mImageCount < 10) {
                                mImageCount++;
                                mPhotoUriList.add(vehicleReportModelEntity);
                            }
                            break;
                    }
                }
            }
        }

        startTaggingFragment(mPhotoUriList);
    }

    private void calculateFileCount(ArrayList<VehicleReportModelEntity> uriList) {
        if (uriList.size() > 1) {
            for (VehicleReportModelEntity vehicleReportModelEntity :
                    uriList) {
                FileUtils.FileType fileType = getFileType(vehicleReportModelEntity);
                if (fileType != null) {
                    switch (fileType) {
                        case PDF:
                            mPdfCount++;
                            break;
                        case VIDEO:
                            mVideoCount++;
                            break;
                        case IMAGE:
                            mImageCount++;
                            break;
                        default:
                            break;
                    }
                }
            }
        }
    }

    private FileUtils.FileType getFileType(VehicleReportModelEntity vehicleReportModelEntity) {
        if (vehicleReportModelEntity.getLocalUri() != null) {
            String uri = vehicleReportModelEntity.getLocalUri().getPath();
            if (FileUtils.getFileType(uri) == PDF) {
                return PDF;
            } else if (FileUtils.getFileType(uri) == VIDEO) {
                return VIDEO;
            } else {
                return IMAGE;
            }
        } else if (!TextUtils.isEmpty(vehicleReportModelEntity.getDocUrl())) {
            if (vehicleReportModelEntity.getType() != null && vehicleReportModelEntity.getType().equalsIgnoreCase("pdf")) {
                return PDF;
            } else if (vehicleReportModelEntity.getType() != null && vehicleReportModelEntity.getType().equalsIgnoreCase("video")) {
                return VIDEO;
            } else {
                return IMAGE;
            }
        }
        return null;
    }

    private void startTaggingFragment(ArrayList<VehicleReportModelEntity> mPhotoUriList) {
        if (mPhotoUriList.size() > (mOldIndex + 1)) {
            if (mEditAddNotes != null) {
                String text = mEditAddNotes.getText().toString().trim();
                final VehicleReportTaggingFragment vehicleReportTaggingFragment =
                        VehicleReportTaggingFragment.newInstance(mDispatcherId, isEditable, mPhotoUriList, text, mOldIndex, mFirebaseData, getArguments().getBoolean(NccConstants.IS_IT_FROM_HISTORY));
                mHomeActivity.push(vehicleReportTaggingFragment);
            }
        }

    }

    @Override
    public void onSave() {
        UiUtils.hideKeyboard(mHomeActivity);
        if (Utils.isNetworkAvailable()) {
            showProgress();
            mHomeActivity.saveDisable();
            if (mPhotoUriList != null && mPhotoUriList.get(0).getDocUrl() == null && mPhotoUriList.get(0).getLocalUri() == null) {
                mPhotoUriList.remove(0);
            }
            TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
                @Override
                public void onRefreshSuccess() {
                    if (isAdded()) {
                        removeFilesFromStorageIfNeeded();
                    }
                }

                @Override
                public void onRefreshFailure() {
                    hideProgress();
                    if(isAdded()) {
                        mHomeActivity.tokenRefreshFailed();
                    }
                }
            });
        } else {
            mUserError.title = ""; mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }

    }

    private void removeFilesFromStorageIfNeeded() {
        pendingDeleteCount = 0;
        for (VehicleReportModelEntity vehicleReportModelEntity :
                mFirebaseData) {
            if (isDataDeleted(vehicleReportModelEntity) && !TextUtils.isEmpty(vehicleReportModelEntity.getDocUrl())) {
                showProgress();
                pendingDeleteCount++;
                isFilesRemoved = true;
                FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();
                StorageReference docRef = firebaseStorage.getReferenceFromUrl(vehicleReportModelEntity.getDocUrl());

                RxFirebaseStorage.delete(docRef).subscribe(() -> {
                    hideProgress();
                    pendingDeleteCount--;
                    if (pendingDeleteCount == 0) {
                        uploadThumbNailFiles();
                    }
                }, throwable -> {
                    hideProgress();
                    if (isAdded()) {
                        mHomeActivity.mintlogEvent("Vehicle Report Delete Document Firebase Database Error - " + throwable.getLocalizedMessage());
                        showError(BaseActivity.Companion.getERROR_SHOW_TYPE_TOAST(), new UserError("", "", getString(R.string.error_failed_to_delete_documents)));
                    }
                });

                if (vehicleReportModelEntity.getThumbUrl() != null) {
                    StorageReference thumbRef = firebaseStorage.getReferenceFromUrl(vehicleReportModelEntity.getThumbUrl());
                    pendingDeleteCount++;
                    RxFirebaseStorage.delete(thumbRef).subscribe(() -> {
                        hideProgress();
                        pendingDeleteCount--;
                        if (pendingDeleteCount == 0) {
                            uploadThumbNailFiles();
                        }
                    }, throwable -> {
                        hideProgress();
                        if (isAdded()) {
                            mHomeActivity.mintlogEvent("Vehicle Report Delete Thumbnail Firebase Database Error - " + throwable.getLocalizedMessage());
                            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_TOAST(), new UserError("", "", getString(R.string.error_failed_to_delete_documents)));
                        }
                    });
                }
            }
        }

        if (pendingDeleteCount == 0) {
            uploadThumbNailFiles();
        }
    }

    private void uploadThumbNailFiles() {
        updateCount = 0;
        fileCount = 0;
        FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();
        for (VehicleReportModelEntity vehicleReportModelEntity :
                mPhotoUriList) {
            if (vehicleReportModelEntity.getLocalUri() != null && (vehicleReportModelEntity.getType().equalsIgnoreCase("pdf") || vehicleReportModelEntity.getType().equalsIgnoreCase("video"))) {
                showProgress();

                String filePath = "";
                if (vehicleReportModelEntity.getType().equalsIgnoreCase("pdf") ) {
                    filePath = Utils.storeImage(mHomeActivity, Utils.generateImageFromPdf(mHomeActivity,
                            vehicleReportModelEntity.getLocalUri()));
                } else {
                    filePath = vehicleReportModelEntity.getThumbUrl();
                }
                fileCount++;

                StorageReference storageReference = firebaseStorage.getReference()
                        .child("Documents/" + mDispatcherId + "/VehicleReport/" + System.currentTimeMillis()
                                + filePath.substring(filePath.lastIndexOf(".")));
                RxFirebaseStorage.putFile(storageReference, Uri.fromFile(new File(filePath))).subscribe(taskSnapshot -> {
                    if (taskSnapshot.getDownloadUrl() != null) {
                        vehicleReportModelEntity.setThumbUrl(taskSnapshot.getDownloadUrl().toString());
                        updateCount++;
                    }
                    if (updateCount == fileCount) {
                        updateCount = 0;
                        fileCount = 0;
                        compressFiles();
                    }

                }, throwable -> {
                    hideProgress();
                    if (isAdded()) {
                        mHomeActivity.mintlogEvent("Vehicle Report Upload Thumbnail Document Firebase Database Error - " + throwable.getLocalizedMessage());
                        showError(BaseActivity.Companion.getERROR_SHOW_TYPE_TOAST(), new UserError("", "", getString(R.string.error_failed_to_upload_documents)));
                    }
                });
            }
        }
        if (updateCount == fileCount) {
            updateCount = 0;
            fileCount = 0;
            compressFiles();
        }
    }

    private void compressFiles() {
        //new CompressFilesTask().execute();

        showProgress();
        disposablesCompressor.add(Observable.fromCallable(() -> {
            try {
                for (VehicleReportModelEntity vehicleReportModelEntity :
                        mPhotoUriList) {
                    if (vehicleReportModelEntity.getLocalUri() != null && vehicleReportModelEntity.getType() != null && vehicleReportModelEntity.getType().equalsIgnoreCase("image")) {
                        vehicleReportModelEntity.setLocalUri(compressImage(vehicleReportModelEntity.getLocalUri()));
                    }
                }
            } catch (Exception e){
                e.printStackTrace();
            }
            return false;
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((result) -> {
                    if(isAdded()){
                        hideProgress();
                        uploadFiles();
                    }
                },Throwable::printStackTrace));
    }

    private void uploadFiles() {
        boolean isModelUploaded = false;
        FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();
        for (VehicleReportModelEntity vehicleReportModelEntity :
                mPhotoUriList) {
            if (vehicleReportModelEntity.getLocalUri() != null && !TextUtils.isEmpty(vehicleReportModelEntity.getLocalUri().getScheme())) {
                showProgress();
                fileCount++;
                isModelUploaded = true;
                String filePath = Utils.getFileName(mHomeActivity.getContentResolver(), (vehicleReportModelEntity.getLocalUri()));
                if(!TextUtils.isEmpty(filePath) && filePath.contains(".")){
                    StorageReference storageReference = firebaseStorage.getReference()
                            .child("Documents/" + mDispatcherId + "/VehicleReport/" + System.currentTimeMillis()
                                    + filePath.substring(filePath.lastIndexOf(".")));

                    mHomeActivity.grantUriPermission("com.agero.ncc",vehicleReportModelEntity.getLocalUri(),Intent.FLAG_GRANT_READ_URI_PERMISSION);

                    RxFirebaseStorage.putFile(storageReference, vehicleReportModelEntity.getLocalUri()).subscribe(taskSnapshot -> {
                        if (taskSnapshot.getDownloadUrl() != null && vehicleReportModelEntity != null) {
                            String path = FileUtils.getUriRealPath(mHomeActivity,vehicleReportModelEntity.getLocalUri());
                            if(!TextUtils.isEmpty(path)){
                                boolean isSuccess = deleteFile(new File(path));
                            }
                            vehicleReportModelEntity.setLocalUri(null);
                            vehicleReportModelEntity.setDocUrl(taskSnapshot.getDownloadUrl().toString());
                            updateCount++;
                        }
                        if (updateCount == fileCount) {
                            MediaScannerConnection.scanFile(mHomeActivity, new String[] { Environment.getExternalStorageDirectory().toString() }, null, new MediaScannerConnection.OnScanCompletedListener() {
                                /*
                                 *   (non-Javadoc)
                                 * @see android.media.MediaScannerConnection.OnScanCompletedListener#onScanCompleted(java.lang.String, android.net.Uri)
                                 */
                                public void onScanCompleted(String path, Uri uri)
                                {
                                    Log.i("ExternalStorage", "Scanned " + path + ":");
                                    Log.i("ExternalStorage", "-> uri=" + uri);
                                }
                            });
                            uploadModelData();
                        }

                    }, throwable -> {
                        hideProgress();
                        if (isAdded()) {
                            mHomeActivity.mintlogEvent("Vehicle Report Upload Document Firebase Database Error - " + throwable.getLocalizedMessage());
                            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_TOAST(), new UserError("", "", getString(R.string.error_failed_to_upload_documents)));
                        }
                    });
                }
            }
        }
        if (!isModelUploaded && (isFilesRemoved || isTagChanged() || !mServerNotes.equalsIgnoreCase(mEditAddNotes.getText().toString().trim()))) {
            uploadModelData();
        }
    }


    private void uploadModelData() {
        if (isAdded()) {
            deleteFile(mHomeActivity.getExternalFilesDir(Environment.DIRECTORY_PICTURES));
            showProgress();
            String text = mEditAddNotes.getText().toString().trim();
            String addedDate = DateTimeUtils.getUtcTimeFormat();
            if (TextUtils.isEmpty(text)) {
                text = null;
            }
            if(text == null && (mPhotoUriList == null || mPhotoUriList.size() == 0)){
                addedDate = null;
            }

            VehicleReportModel vehicleReportModel = new VehicleReportModel(text, addedDate, mPhotoUriList);
//        mHomeActivity.mintlogEvent(new Gson().toJson(vehicleReportModel));

            HashMap<String, String> extraDatas = new HashMap<>();
            extraDatas.put("json", new Gson().toJson(vehicleReportModel));
            extraDatas.put("dispatch_id", mDispatcherId);
            mHomeActivity.mintlogEventExtraData("Add Photos", extraDatas);

            RxFirebaseDatabase.setValue(modelReference, vehicleReportModel).subscribe(() -> {
                addSelectPhoto();
                hideProgress();
                mHomeActivity.onBackPressed();
            }, throwable -> {
                hideProgress();
                if (isAdded()) {
                    mHomeActivity.mintlogEvent("Vehicle Report Upload Model Firebase Database Error - " + throwable.getLocalizedMessage());
                }
            });
        }

    }

    private void setTextWatcherForNotes() {
        mEditAddNotes.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                enableDisableSave();
            }
        });
    }

    private void enableDisableSave() {
        if (mEditAddNotes != null) {
            String text = mEditAddNotes.getText().toString().trim();
            if ((!mServerNotes.equalsIgnoreCase(text) || (mPhotoUriList.size() > 1 && !mServerNotes.equalsIgnoreCase(text)) || isNewFilesPresent() || isTagChanged() || isServerDataDeleted())) {
                mHomeActivity.saveEnable();
            } else {
                mHomeActivity.saveDisable();
            }
        } else {
            if (mHomeActivity != null) {
                mHomeActivity.saveDisable();
            }
        }
    }

    private boolean isServerDataDeleted() {
        boolean isServerDataDeleted = false;
        for (VehicleReportModelEntity vehicleReportModelEntity :
                mFirebaseData) {
            if (isDataDeleted(vehicleReportModelEntity)) {
                isServerDataDeleted = true;
            }
        }
        return isServerDataDeleted;
    }

    private boolean isDataDeleted(VehicleReportModelEntity vehicleReportModelEntity) {
        boolean isServerDataDeleted = true;
        for (VehicleReportModelEntity vehicleReportModelEntity1 :
                mPhotoUriList) {
            if (vehicleReportModelEntity.getDocUrl() != null && vehicleReportModelEntity.getDocUrl().equalsIgnoreCase(vehicleReportModelEntity1.getDocUrl())) {
                isServerDataDeleted = false;
            }
        }
        return isServerDataDeleted;
    }

    private boolean isTagChanged() {
        boolean isTagChanged = false;
        int incCount = 0;

        if (mPhotoUriList.size() > 0 && mPhotoUriList.get(0).getLocalUri() == null && mPhotoUriList.get(0).getDocUrl() == null) {
            incCount = 1;
        }

        if (mFirebaseData != null) {
            for (int i = 0; i < mFirebaseData.size(); i++) {
                if (mPhotoUriList.size() > i + incCount) {
                    VehicleReportModelEntity vehicleReportModelEntity = mPhotoUriList.get(i + incCount);
                    if (checkIsTagChange(vehicleReportModelEntity, mFirebaseData.get(i))) {
                        isTagChanged = true;
                    }
                }
            }
        }
        return isTagChanged;
    }

    private boolean checkIsTagChange(VehicleReportModelEntity vehicleReportModelEntity, VehicleReportModelEntity vehicleReportModelEntity1) {
        boolean isTagChanged = false;
        if (!(vehicleReportModelEntity.getTags().get(0).equalsIgnoreCase(vehicleReportModelEntity1.getTags().get(0)))) {
            isTagChanged = true;
        } else if (!(vehicleReportModelEntity.getTags().get(1).equalsIgnoreCase(vehicleReportModelEntity1.getTags().get(1)))) {
            isTagChanged = true;
        }
        return isTagChanged;
    }


    private boolean isNewFilesPresent() {
        if (mPhotoUriList == null || mPhotoUriList.size() == 1) {
            return false;
        } else {
            boolean isNewFilePresent = false;
            for (VehicleReportModelEntity vehicleReportModelEntity :
                    mPhotoUriList) {
                if (vehicleReportModelEntity.getLocalUri() != null) {
                    isNewFilePresent = true;
                }
            }
            return isNewFilePresent;
        }
    }

    class CompressFilesTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress();
        }

        @Override
        protected Void doInBackground(Void... params) {
            for (VehicleReportModelEntity vehicleReportModelEntity :
                    mPhotoUriList) {
                if (vehicleReportModelEntity.getLocalUri() != null && vehicleReportModelEntity.getType() != null && vehicleReportModelEntity.getType().equalsIgnoreCase("image")) {
                  vehicleReportModelEntity.setLocalUri(compressImage(vehicleReportModelEntity.getLocalUri()));
                }
            }
            return null;
        }

        protected void onPostExecute(Void bitmap) {
            hideProgress();
            uploadFiles();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(!disposablesCompressor.isDisposed()){
            disposablesCompressor.dispose();
        }
    }
}
