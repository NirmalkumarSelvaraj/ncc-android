package com.agero.ncc.utils;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;

import com.agero.ncc.R;
import com.agero.ncc.app.NCCApplication;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.model.LatLng;
import com.shockwave.pdfium.PdfDocument;
import com.shockwave.pdfium.PdfiumCore;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import timber.log.Timber;

public class Utils {


    public static boolean isGooglePlayServicesAvailable(Context context) {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int status = googleAPI.isGooglePlayServicesAvailable(context);
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            googleAPI.getErrorDialog((Activity) context, status, 0).show();
            return false;
        }
    }


    public static boolean isNetworkAvailable() {
        ConnectivityManager mConnectivityManager =
                (ConnectivityManager) NCCApplication.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = mConnectivityManager.getActiveNetworkInfo();
        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
    }

    public static boolean isGpsEnabled(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                int locationSetting = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);
                switch (locationSetting) {
                    case Settings.Secure.LOCATION_MODE_OFF:
                        Timber.d("Location setting: Off");
                        break;
                    case Settings.Secure.LOCATION_MODE_SENSORS_ONLY:
                        Timber.d("Location setting: GPS only");
                        return true;
                    case Settings.Secure.LOCATION_MODE_BATTERY_SAVING:
                        Timber.d("Location setting: Battery saving");
                        return true;
                    case Settings.Secure.LOCATION_MODE_HIGH_ACCURACY:
                        Timber.d("Location setting: High accuracy");
                        return true;
                }
            } catch (Settings.SettingNotFoundException snfe) {
                Timber.d("GPS location setting not available on device.");
                snfe.printStackTrace();
            }

            // Otherwise location services must be on, and GPS and network location providers must be on
        } else {
            final LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            Timber.d("GPS_PROVIDER: " + manager.isProviderEnabled(LocationManager.GPS_PROVIDER) + " NETWORK_PROVIDER: " + manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER));
            if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER) && manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                return true;
            }
        }

        return false;
    }

    public static void gotoSettings(Context context) {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.parse("package:" + context.getPackageName());
        intent.setData(uri);
        context.startActivity(intent);
    }

    public static boolean isHighAccuGpsEnabled(Context context) {
        try {
            int locationSetting = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);
            switch (locationSetting) {
                case Settings.Secure.LOCATION_MODE_OFF:
                    Timber.d("Location setting: Off");
                    break;
                case Settings.Secure.LOCATION_MODE_SENSORS_ONLY:
                    Timber.d("Location setting: GPS only");
                    break;
                case Settings.Secure.LOCATION_MODE_BATTERY_SAVING:
                    Timber.d("Location setting: Battery saving");
                    break;
                case Settings.Secure.LOCATION_MODE_HIGH_ACCURACY:
                    Timber.d("Location setting: High accuracy");
                    return true;
            }
        } catch (Settings.SettingNotFoundException snfe) {
            Timber.d("GPS location setting not available on device.");
            snfe.printStackTrace();
        }
        return false;
    }

    public static String getDeviceDensity(Context context) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        switch (metrics.densityDpi) {
            case 160:
                return "mdpi";
            case 240:
                return "hdpi";
            case 320:
                return "xhdpi";
            case 480:
                return "xxhdpi";
            case 640:
                return "xxxhdpi";
        }
        return "xxhdpi";
    }

    public static String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";
        String mode = "mode=driving";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + mode;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

        return url;
    }

    /**
     * To get JSON response from directions API
     *
     * @param strUrl directions API url
     * @return response string
     * @throws IOException
     */
    public static String getDirectionsResponse(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.connect();

            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            if (iStream != null) {
                iStream.close();
            }
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return data;
    }

    public static Address getGeoCodedAddress(Context context, String address) {
        try {
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            return geocoder.getFromLocationName(address, 1).get(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Address getReverseGeoCodedAddress(Context context, double latitude, double longitude) {
        try {
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            return geocoder.getFromLocation(latitude, longitude, 1).get(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getStatesAbbreviation(String stateName) {
        Map<String, String> states = new HashMap<>();
        states.put("Alabama", "AL");
        states.put("Alaska", "AK");
        states.put("Alberta", "AB");
        states.put("American Samoa", "AS");
        states.put("Arizona", "AZ");
        states.put("Arkansas", "AR");
        states.put("Armed Forces (AE)", "AE");
        states.put("Armed Forces Americas", "AA");
        states.put("Armed Forces Pacific", "AP");
        states.put("British Columbia", "BC");
        states.put("California", "CA");
        states.put("Colorado", "CO");
        states.put("Connecticut", "CT");
        states.put("Delaware", "DE");
        states.put("District Of Columbia", "DC");
        states.put("Florida", "FL");
        states.put("Georgia", "GA");
        states.put("Guam", "GU");
        states.put("Hawaii", "HI");
        states.put("Idaho", "ID");
        states.put("Illinois", "IL");
        states.put("Indiana", "IN");
        states.put("Iowa", "IA");
        states.put("Kansas", "KS");
        states.put("Kentucky", "KY");
        states.put("Louisiana", "LA");
        states.put("Maine", "ME");
        states.put("Manitoba", "MB");
        states.put("Maryland", "MD");
        states.put("Massachusetts", "MA");
        states.put("Michigan", "MI");
        states.put("Minnesota", "MN");
        states.put("Mississippi", "MS");
        states.put("Missouri", "MO");
        states.put("Montana", "MT");
        states.put("Nebraska", "NE");
        states.put("Nevada", "NV");
        states.put("New Brunswick", "NB");
        states.put("New Hampshire", "NH");
        states.put("New Jersey", "NJ");
        states.put("New Mexico", "NM");
        states.put("New York", "NY");
        states.put("Newfoundland", "NF");
        states.put("North Carolina", "NC");
        states.put("North Dakota", "ND");
        states.put("Northwest Territories", "NT");
        states.put("Nova Scotia", "NS");
        states.put("Nunavut", "NU");
        states.put("Ohio", "OH");
        states.put("Oklahoma", "OK");
        states.put("Ontario", "ON");
        states.put("Oregon", "OR");
        states.put("Pennsylvania", "PA");
        states.put("Prince Edward Island", "PE");
        states.put("Puerto Rico", "PR");
        states.put("Quebec", "PQ");
        states.put("Rhode Island", "RI");
        states.put("Saskatchewan", "SK");
        states.put("South Carolina", "SC");
        states.put("South Dakota", "SD");
        states.put("Tennessee", "TN");
        states.put("Texas", "TX");
        states.put("Utah", "UT");
        states.put("Vermont", "VT");
        states.put("Virgin Islands", "VI");
        states.put("Virginia", "VA");
        states.put("Washington", "WA");
        states.put("West Virginia", "WV");
        states.put("Wisconsin", "WI");
        states.put("Wyoming", "WY");
        states.put("Yukon Territory", "YT");
        return states.get(stateName);
    }

    public static String getFileName(ContentResolver resolver, Uri uri) {
        String fileName = null;
        if (uri.getScheme().equals("file")) {
            fileName = uri.getLastPathSegment();
        } else {
            Cursor cursor = null;
            try {
                cursor = resolver.query(uri, new String[]{
                        MediaStore.Images.ImageColumns.DISPLAY_NAME
                }, null, null, null);

                if (cursor != null && cursor.moveToFirst()) {
                    fileName = cursor.getString(cursor.getColumnIndex(MediaStore.Images.ImageColumns.DISPLAY_NAME));
                }
            } finally {

                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return fileName;
    }

    public static void makePhoneCall(Context context, String call) {
        if(context != null && call != null) {
            Intent intent = new Intent(Intent.ACTION_CALL);
            intent.setData(Uri.parse("tel:" + call));
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.

                return;
            }
            context.startActivity(intent);
        }
    }

    public static boolean isCallSupported(Context context){
        try {
            TelephonyManager telMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (telMgr.getPhoneType() == TelephonyManager.PHONE_TYPE_NONE) {
                return false;
            } else {
                return true;
            }
        } catch (Exception e){

        }

        return false;
    }

    public static boolean isValidPassword(String password) {
        Pattern pattern;
        Matcher matcher;
        String PASSWORD_PATTERN = "^((?=.*[A-Z])(?=.*[a-z])(?=.*\\d)|(?=.*[a-z])(?=.*\\d)" +
                "(?=.*[\\!\\@\\#\\$\\%\\^\\&\\*])|(?=.*[A-Z])(?=.*\\d)(?=.*[\\!\\@\\#\\$\\%\\^\\&\\*])|(?=.*[A-Z])(?=.*[a-z])(?=.*[\\!\\@\\#\\$\\%\\^\\&\\*])).{8,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);
        return matcher.matches();
    }

    /**
     * To get bitmap from single selected pdf.
     *
     * @param pdfUri
     * @return Uri
     */
    public static Bitmap generateImageFromPdf(Context context, Uri pdfUri) {
        int pageNumber = 0;
        PdfiumCore pdfiumCore = new PdfiumCore(context);
        String path = null;
        try {
            ParcelFileDescriptor fd = context.getContentResolver().openFileDescriptor(pdfUri, "r");
            PdfDocument pdfDocument = pdfiumCore.newDocument(fd);
            pdfiumCore.openPage(pdfDocument, pageNumber);
            int width = pdfiumCore.getPageWidthPoint(pdfDocument, pageNumber);
            int height = pdfiumCore.getPageHeightPoint(pdfDocument, pageNumber);
            Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            pdfiumCore.renderPageBitmap(pdfDocument, bmp, pageNumber, 0, 0, width, height);
            return bmp;
        } catch (Exception e) {
            return null;
        }

    }

    public static String getDisplayStatusText(Context context, String status) {

        switch (status) {
            case NccConstants.JOB_STATUS_UNASSIGNED:
                status = context.getString(R.string.job_status_unassigned);
                break;
            case NccConstants.JOB_STATUS_ASSIGNED:
                status = context.getString(R.string.job_status_assigned);
                break;
            case NccConstants.JOB_STATUS_EN_ROUTE:
                status = context.getString(R.string.job_status_en_route);
                break;
            case NccConstants.JOB_STATUS_ON_SCENE:
                status = context.getString(R.string.job_status_on_scene);
                break;
            case NccConstants.JOB_STATUS_TOWING:
                status = context.getString(R.string.job_status_towing);
                break;
            case NccConstants.JOB_STATUS_DESTINATION_ARRIVAL:
                status = context.getString(R.string.job_status_destination_arrival);
                break;
            case NccConstants.JOB_STATUS_JOB_COMPLETED:
                status = context.getString(R.string.job_status_job_complete);
                break;
            case NccConstants.JOB_STATUS_EXPIRED:
                status = context.getString(R.string.job_status_expired);
                break;
            case NccConstants.JOB_STATUS_OFFERED:
                status = context.getString(R.string.job_status_offered);
                break;
            case NccConstants.JOB_STATUS_DECLINED:
                status = context.getString(R.string.job_status_declined);
                break;
            case NccConstants.JOB_STATUS_ACCEPTED:
                status = context.getString(R.string.job_status_accepted);
                break;
            case NccConstants.JOB_STATUS_PAUSED:
                status = context.getString(R.string.job_status_paused);
                break;
            case NccConstants.JOB_STATUS_CANCELLED:
                status = context.getString(R.string.job_status_cancelled);
                break;
            case NccConstants.JOB_STATUS_DENIED:
                status = context.getString(R.string.job_status_denied);
                break;
            case NccConstants.JOB_STATUS_REJECTED:
                status = context.getString(R.string.job_status_rejected);
                break;
            case NccConstants.JOB_STATUS_AWARDED:
                status = context.getString(R.string.job_status_awarded);
                break;
            case NccConstants.JOB_STATUS_GOA:
                status = context.getString(R.string.job_status_goa);
                break;
            case NccConstants.JOB_STATUS_UNSUCCESSFUL:
                status = context.getString(R.string.job_status_service_unsuccessful);
                break;
            default:
                status = toCamelCase(status);

        }
        return status;
    }

    public static boolean isUndefinedStatus(String status) {
        boolean isUndefinedStatus = false;
        switch (status) {
            case NccConstants.JOB_STATUS_UNASSIGNED:
            case NccConstants.JOB_STATUS_ASSIGNED:
            case NccConstants.JOB_STATUS_EN_ROUTE:
            case NccConstants.JOB_STATUS_ON_SCENE:
            case NccConstants.JOB_STATUS_TOWING:
            case NccConstants.JOB_STATUS_DESTINATION_ARRIVAL:
            case NccConstants.JOB_STATUS_JOB_COMPLETED:
            case NccConstants.JOB_STATUS_EXPIRED:
            case NccConstants.JOB_STATUS_OFFERED:
            case NccConstants.JOB_STATUS_DECLINED:
                isUndefinedStatus = false;
                break;
            default:
                isUndefinedStatus = true;

        }
        return isUndefinedStatus;
    }

    public static String toCamelCase(String status) {
        if (status == null)
            return "";

        final StringBuilder ret = new StringBuilder(status.length());
        String[] words;

        if (status.contains("_")) {
            words = status.split("_");
        } else {
            words = status.split(" ");
        }

        for (String word : words) {
            if (!word.isEmpty()) {
                ret.append(word.substring(0, 1).toUpperCase());
                ret.append(word.substring(1).toLowerCase());
            }
            if (!(ret.length() == status.length()))
                ret.append(" ");
        }

        return ret.toString();
    }

    //To save bitmap as image.
    public static String storeImage(Context context, Bitmap image) {
        String filePath = "";
        File pictureFile = getOutputMediaFile(context);
        if (pictureFile == null) {
            Log.d("storeImage",
                    "Error creating media file, check storage permissions: ");// e.getMessage());
            return filePath;
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            if(image != null) {
                image.compress(Bitmap.CompressFormat.PNG, 90, fos);
            }
            filePath = pictureFile.getPath();
            fos.close();
        } catch (FileNotFoundException e) {
            Log.d("storeImage", "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d("storeImage", "Error accessing file: " + e.getMessage());
        }
        return filePath;
    }

    /**
     * Create a File for saving an thumbnail
     *
     * @param context
     */
    private static File getOutputMediaFile(Context context) {
        File mediaStorageDir;
        if(context != null) {
             mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                    + "/Android/data/"
                    + context.getPackageName()
                    + "/Files");
        }else{
             mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                    + "/Android/data/com.agero.ncc/Files");
        }

        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        File mediaFile;
        String mImageName = System.currentTimeMillis() + ".jpg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    public static String getRoleText(Context context, List<String> roles) {

        String rolesText = "";
        for (String role : roles) {

            if (role.contains(NccConstants.USER_ROLE_PARTNER_ADMIN)) {
                rolesText = context.getString(R.string.role_admin);
            } else {
                rolesText = context.getString(R.string.role_dispatcher);
            }

            return rolesText;
//            if (role.contains(NccConstants.USER_ROLE_BILLING_MANAGER)) {
//                rolesText += context.getString(R.string.role_billing_manager);
//            } else if (role.contains(NccConstants.USER_ROLE_DISPATCHER)) {
//                rolesText += context.getString(R.string.role_dispatcher);
//            } else if (role.contains(NccConstants.USER_ROLE_TEAM_MANAGER)) {
//                rolesText += context.getString(R.string.role_driver_equipment_manager);
//            } else {
////                rolesText += context.getString(R.string.role_driver);
//            }
        }

        return rolesText;
    }

    public static String getScreenNameForAnalytics(Context context, String className) {
        String screenName = "";

        switch (className) {
            case NccConstants.ANALYTICS_WELCOME_SIGN_IN:
                screenName = context.getString(R.string.landing_screen);
                break;
            case NccConstants.ANALYTICS_CREATE_ACCOUNT:
                screenName = context.getString(R.string.title_createaccount);
                break;
            case NccConstants.ANALYTICS_LEGAL:
                screenName = context.getString(R.string.title_legal);
                break;
            case NccConstants.ANALYTICS_PERMISSION_REQUEST:
                screenName = context.getString(R.string.label_permission_request);
                break;
            case NccConstants.ANALYTICS_SIGN_IN:
                screenName = context.getString(R.string.label_sigin_in);
                break;
            case NccConstants.ANALYTICS_EQUIPMENT_SELCTION:
                screenName = context.getString(R.string.select_equipment);
                break;
            case NccConstants.ANALYTICS_ACTIVE_JOBS:
                screenName = context.getString(R.string.homeicon_active_jobs);
                break;
            case NccConstants.ANALYTICS_MODIFY_STATUS:
                screenName = context.getString(R.string.label_job_modify_status);
                break;
            case NccConstants.ANALYTICS_SCAN_VIN:
                screenName = context.getString(R.string.title_scanvin);
                break;
            case NccConstants.ANALYTICS_ADD_ATTACHMENTS:
                screenName = context.getString(R.string.title_vehicle_condition_report);
                break;
            case NccConstants.ANALYTICS_TAG_ATTACHMENTS:
                screenName = context.getString(R.string.label_tag_attachments);
                break;
            case NccConstants.ANALYTICS_JOB_NOTES:
                screenName = context.getString(R.string.label_job_notes);
                break;
            case NccConstants.ANALYTICS_ASSIGN_DRIVER:
                screenName = context.getString(R.string.label_assign_driver);
                break;
            case NccConstants.ANALYTICS_SERVICE_UNSUCCESSFUL:
                screenName = context.getString(R.string.label_service_unsuccessful);
                break;
            case NccConstants.ANALYTICS_EDIT_SERVICE:
                screenName = context.getString(R.string.dispatcher_job_options_edit_service);
                break;
            case NccConstants.ANALYTICS_JOB_HISTORY:
                screenName = context.getString(R.string.label_history);
                break;
            case NccConstants.ANALYTICS_ALERTS:
                screenName = context.getString(R.string.title_alerts);
                break;
            case NccConstants.ANALYTICS_ACCOUNT:
                screenName = context.getString(R.string.title_account);
                break;
            case NccConstants.ANALYTICS_EDIT_PROFILE:
                screenName = context.getString(R.string.label_edit_profile);
                break;
            case NccConstants.ANALYTICS_UPDATE_PASSWORD:
                screenName = context.getString(R.string.label_update_password);
                break;
            case NccConstants.ANALYTICS_COMPANY:
                screenName = context.getString(R.string.title_facility);
                break;
            case NccConstants.ANALYTICS_HELP:
                screenName = context.getString(R.string.accounts_label_help);
                break;
            default:
                screenName = "";
        }

        return screenName;
    }

    public static boolean isTow(String service) {

        boolean isTow = false;

        if(TextUtils.isEmpty(service)){
            return isTow;
        }
        switch (service) {
            case NccConstants.SERVICE_TYPE_TOW:
            case NccConstants.SERVICE_TYPE_VRTOW:
            case NccConstants.SERVICE_TYPE_HTOW:
            case NccConstants.SERVICE_TYPE_LTOW:
            case NccConstants.SERVICE_TYPE_ASTOW:
            case NccConstants.SERVICE_TYPE_MTOW:
                isTow = true;
                break;
        }

        return isTow;
    }

    public static String getDisplayServiceTypes(Context mContext, String service) {


        String serviceType = mContext.getString(R.string.service_type_other);
        switch (service.toUpperCase(Locale.ENGLISH)) {
            case NccConstants.SERVICE_TYPE_TOW:
                serviceType = mContext.getString(R.string.service_type_tow);
                break;
            case NccConstants.SERVICE_TYPE_VRTOW:
                serviceType = mContext.getString(R.string.service_type_vrtow);
                break;
            case NccConstants.SERVICE_TYPE_HTOW:
                serviceType = mContext.getString(R.string.service_type_htow);
                break;
            case NccConstants.SERVICE_TYPE_LTOW:
                serviceType = mContext.getString(R.string.service_type_ltow);
                break;
            case NccConstants.SERVICE_TYPE_ASTOW:
                serviceType = mContext.getString(R.string.service_type_astow);
                break;
            case NccConstants.SERVICE_TYPE_MTOW:
                serviceType = mContext.getString(R.string.service_type_mtow);
                break;
            case NccConstants.SERVICE_TYPE_ROADSIDE:
                serviceType = mContext.getString(R.string.service_type_roadside);
                break;
            case NccConstants.SERVICE_TYPE_WINCH:
                serviceType = mContext.getString(R.string.service_type_winch);
                break;
            case NccConstants.SERVICE_TYPE_MWNH:
                serviceType = mContext.getString(R.string.service_type_mwnh);
                break;
            case NccConstants.SERVICE_TYPE_HFLD:
                serviceType = mContext.getString(R.string.service_type_hfld);
                break;
            case NccConstants.SERVICE_TYPE_LFLD:
                serviceType = mContext.getString(R.string.service_type_lfld);
                break;
            case NccConstants.SERVICE_TYPE_LFLT:
                serviceType = mContext.getString(R.string.service_type_lflt);
                break;
            case NccConstants.SERVICE_TYPE_LWNH:
                serviceType = mContext.getString(R.string.service_type_lwnh);
                break;
            case NccConstants.SERVICE_TYPE_LKTSMITH:
                serviceType = mContext.getString(R.string.service_type_lktsmith);
                break;
            case NccConstants.SERVICE_TYPE_MCPK:
                serviceType = mContext.getString(R.string.service_type_mcpk);
                break;
            case NccConstants.SERVICE_TYPE_HJMS:
                serviceType = mContext.getString(R.string.service_type_hjms);
                break;
            case NccConstants.SERVICE_TYPE_LJMS:
                serviceType = mContext.getString(R.string.service_type_ljms);
                break;
            case NccConstants.SERVICE_TYPE_MFLD:
                serviceType = mContext.getString(R.string.service_type_mfld);
                break;
            case NccConstants.SERVICE_TYPE_MFLT:
                serviceType = mContext.getString(R.string.service_type_mflt);
                break;
            case NccConstants.SERVICE_TYPE_MJMS:
                serviceType = mContext.getString(R.string.service_type_mjms);
                break;
            case NccConstants.SERVICE_TYPE_HFLT:
                serviceType = mContext.getString(R.string.service_type_hflt);
                break;
            case NccConstants.SERVICE_TYPE_VSTG:
                serviceType = mContext.getString(R.string.service_type_vstg);
                break;
            case NccConstants.SERVICE_TYPE_LKT:
                serviceType = mContext.getString(R.string.service_type_lkt);
                break;
            case NccConstants.SERVICE_TYPE_HWNH:
                serviceType = mContext.getString(R.string.service_type_hwnh);
                break;
            default:
                serviceType = mContext.getString(R.string.service_type_other);
                break;
        }

        return serviceType;
    }
}