package com.agero.ncc.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Icon;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;

import com.agero.ncc.R;
import com.agero.ncc.activities.ChatActivity;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.app.NCCApplication;
import com.agero.ncc.db.database.NccDatabase;
import com.agero.ncc.db.entity.Alert;
import com.agero.ncc.utils.NccConstants;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.splunk.mint.Mint;
import com.splunk.mint.MintLogLevel;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.inject.Inject;

import co.chatsdk.core.session.NM;
import co.chatsdk.ui.manager.BaseInterfaceAdapter;
import co.chatsdk.ui.manager.InterfaceManager;
import co.chatsdk.ui.utils.AppBackgroundMonitor;


public class NccFirebaseMessagingService extends FirebaseMessagingService {
    @Inject
    public SharedPreferences mPrefs;
    @Inject
    public NccDatabase nccDatabase;
    public SharedPreferences.Editor mEditor;
    private static final String TAG = NccFirebaseMessagingService.class.getName();

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        NCCApplication.getContext().getComponent().inject(this);

        if (remoteMessage != null && remoteMessage.getNotification() != null && remoteMessage.getData() != null) {

            Map<String, String> extras = remoteMessage.getData();

            String notificationType = extras.get(NccConstants.NOTIFICATION_TYPE);

            if (NccConstants.NOTIFICATION_TYPE_CHAT.equalsIgnoreCase(notificationType)) {
                sendChatNotification(this, remoteMessage);
            } else {
                sendNotification(this, remoteMessage);
            }
        } else {
            sendNotification(this, remoteMessage);
        }
    }

    private void sendNotification(Context context, RemoteMessage remoteMessage) {

        HashMap<String, String> extraDatas = new HashMap<>();
        extraDatas.put("message json", new Gson().toJson(remoteMessage.getNotification()));
        extraDatas.put("payload json", new Gson().toJson(remoteMessage.getData()));
        mintlogEventExtraData("Alert-Notification", extraDatas);

        populateAsync(nccDatabase, remoteMessage);

        Intent gcm_rec = new Intent(NccConstants.ALERT_ACTION);
        LocalBroadcastManager.getInstance(this).sendBroadcast(gcm_rec);

        String sound = "ringtone";

        if(!TextUtils.isEmpty(remoteMessage.getNotification().getSound()) && !"default".equalsIgnoreCase(remoteMessage.getNotification().getSound())){
            sound = remoteMessage.getNotification().getSound();
        }

        int uriId = getResources().getIdentifier(sound, "raw", getPackageName());
        Uri path = Uri.parse("android.resource://" + getPackageName() + "/" + uriId);

        Intent appIntent = new Intent(context, HomeActivity.class);
        appIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        if (remoteMessage.getData().size() > 0) {
            appIntent.putExtra(NccConstants.INTENT_NOTIFICATION_KEY_DRIVER_ID, remoteMessage.getData().get("dispatchAssignedToId"));
            appIntent.putExtra(NccConstants.INTENT_NOTIFICATION_KEY_VENDOR_ID, remoteMessage.getData().get("vendorId"));
        }

        createNotication(remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody(), path, appIntent);

    }

    private void sendChatNotification(Context context, RemoteMessage remoteMessage) {
        HashMap<String, String> extraDatas = new HashMap<>();

        extraDatas.put("message json", new Gson().toJson(remoteMessage.getNotification()));
        extraDatas.put("payload json", new Gson().toJson(remoteMessage.getData()));
        mintlogEventExtraData("Chat-Notification", extraDatas);

        Intent nccBadgeUpdateService = new Intent(this,NCCBadgeUpdateService.class);
        startService(nccBadgeUpdateService);


        String sound = "ringtone";

        if(!TextUtils.isEmpty(remoteMessage.getNotification().getSound()) && !"default".equalsIgnoreCase(remoteMessage.getNotification().getSound())){
            sound = remoteMessage.getNotification().getSound();
        }

        int uriId = getResources().getIdentifier(sound, "raw", getPackageName());
        Uri path = Uri.parse("android.resource://" + getPackageName() + "/" + uriId);

        Intent appIntent = new Intent(context, ChatActivity.class);
        ChatActivity.toUserName = "";
        ChatActivity.isFromJobDetail = false;

        if (remoteMessage.getData().size() > 0) {

            String threadEntityID = remoteMessage.getData().get(NccConstants.NOTIFICATION_ENTITY_ID);
            appIntent.putExtra(BaseInterfaceAdapter.THREAD_ENTITY_ID, threadEntityID);
            appIntent.setAction(threadEntityID);
        }
        appIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        createNotication(remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody(), path, appIntent);


    }

    private void createNotication(String title, String message, Uri soundPath, Intent appIntent) {

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, appIntent,
                PendingIntent.FLAG_ONE_SHOT);

        Notification.Builder notificationBuilder =
                (Notification.Builder) new Notification.Builder(this);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            Bitmap bitMap = BitmapFactory.decodeResource(getResources(), R.mipmap.icon_notifications);
            Icon icon = Icon.createWithBitmap(bitMap);
            notificationBuilder.setLargeIcon(icon);
            notificationBuilder.setSmallIcon(icon);
        } else {
            notificationBuilder.setSmallIcon(R.mipmap.icon_notifications);
        }

        notificationBuilder.setContentTitle(title);
        notificationBuilder.setContentText(message);
        notificationBuilder.setAutoCancel(true);
        notificationBuilder.setSound(mPrefs.getString(NccConstants.PREF_KEY_NOTIFICATION_URI, "").isEmpty()
                ? soundPath : Uri.parse(mPrefs.getString(NccConstants.PREF_KEY_NOTIFICATION_URI, "")));
        notificationBuilder.setPriority(Notification.PRIORITY_HIGH);
        notificationBuilder.setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O && notificationManager != null) {
            String channelID = "com.agero.ncc.services.NccFirebaseMessagingService";// The id of the channel.
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(channelID, "MyApp", importance);
            // Create a notification and set the notification channel.
            Notification notification = notificationBuilder
                    .setChannelId(channelID)
                    .build();

            notificationManager.createNotificationChannel(mChannel);
            notificationManager.notify(new Random().nextInt(), notification);
        } else if (notificationManager != null) {
            notificationManager.notify(new Random().nextInt() /* ID of notification */,
                    notificationBuilder.build());
        }

    }

    public static void populateAsync(@NonNull final NccDatabase db, RemoteMessage rm) {
        PopulateDbAsync task = new PopulateDbAsync(db, rm);
        task.execute();
    }

    private static Alert addUser(final NccDatabase db, Alert alert) {
        db.getAlertDao().insertAll(alert);
        return alert;
    }


    public void mintlogEventExtraData(String event, HashMap<String, String> extraDatas) {

        if (extraDatas != null) {
            for (Map.Entry<String, String> extraData :
                    extraDatas.entrySet()) {
                Mint.addExtraData(extraData.getKey(), extraData.getValue());
            }
        } else {
            Mint.clearExtraData();
        }

        if (!TextUtils.isEmpty(mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""))) {
            Mint.setUserIdentifier(mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""));
            Mint.addExtraData("user_id", mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""));
        }
        if (!TextUtils.isEmpty(mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, ""))) {
            Mint.addExtraData("facility_id", mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, ""));
        }

        Mint.logEvent(event, MintLogLevel.Debug);
        Mint.flush();
    }

    private static void populateWithData(NccDatabase db, RemoteMessage remoteMessage) {
        Alert alert = new Alert();
        alert.setAlertId(System.currentTimeMillis());
        alert.setAlertTitle(remoteMessage.getNotification().getTitle());
        alert.setAlertDescription(remoteMessage.getNotification().getBody());
        alert.setUpdatedTime(new Date());
        alert.setRead(false);
        addUser(db, alert);
    }

    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

        private final NccDatabase mNccDatabase;
        private final RemoteMessage mRemoteMessage;

        PopulateDbAsync(NccDatabase db, RemoteMessage rm) {
            mNccDatabase = db;
            mRemoteMessage = rm;
        }

        @Override
        protected Void doInBackground(final Void... params) {
            populateWithData(mNccDatabase, mRemoteMessage);
            return null;
        }

    }
}
