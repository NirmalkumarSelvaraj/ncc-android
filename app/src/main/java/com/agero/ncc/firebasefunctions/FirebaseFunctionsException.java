package com.agero.ncc.firebasefunctions;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.SparseArray;
import com.google.firebase.FirebaseException;
import org.json.JSONException;
import org.json.JSONObject;

public class FirebaseFunctionsException extends FirebaseException {
    @NonNull
    private final FirebaseFunctionsException.Code code;
    @Nullable
    private final Object details;

    @Nullable
    static FirebaseFunctionsException fromResponse(FirebaseFunctionsException.Code code, @Nullable String body, Serializer serializer) {
        String description = code.name();
        Object details = null;

        try {
            JSONObject json = new JSONObject(body);
            JSONObject error = json.getJSONObject("error");
            if (error.opt("status") instanceof String) {
                code = FirebaseFunctionsException.Code.valueOf(error.getString("status"));
                description = code.name();
            }

            if (error.opt("message") instanceof String) {
                description = error.getString("message");
            }

            details = error.opt("details");
            if (details != null) {
                details = serializer.decode(details);
            }
        } catch (IllegalArgumentException var7) {
            code = FirebaseFunctionsException.Code.INTERNAL;
            description = code.name();
        } catch (JSONException var8) {
            ;
        }

        return code == FirebaseFunctionsException.Code.OK ? null : new FirebaseFunctionsException(description, code, details);
    }

    FirebaseFunctionsException(@NonNull String message, @NonNull FirebaseFunctionsException.Code code, @Nullable Object details) {
        super(message);
        this.code = code;
        this.details = details;
    }

    FirebaseFunctionsException(@NonNull String message, @NonNull FirebaseFunctionsException.Code code, @Nullable Object details, Throwable cause) {
        super(message, cause);
        this.code = code;
        this.details = details;
    }

    @NonNull
    public FirebaseFunctionsException.Code getCode() {
        return this.code;
    }

    @Nullable
    public Object getDetails() {
        return this.details;
    }

    public static enum Code {
        OK(0),
        CANCELLED(1),
        UNKNOWN(2),
        INVALID_ARGUMENT(3),
        DEADLINE_EXCEEDED(4),
        NOT_FOUND(5),
        ALREADY_EXISTS(6),
        PERMISSION_DENIED(7),
        RESOURCE_EXHAUSTED(8),
        FAILED_PRECONDITION(9),
        ABORTED(10),
        OUT_OF_RANGE(11),
        UNIMPLEMENTED(12),
        INTERNAL(13),
        UNAVAILABLE(14),
        DATA_LOSS(15),
        UNAUTHENTICATED(16);

        private final int value;
        private static final SparseArray<FirebaseFunctionsException.Code> STATUS_LIST = buildStatusList();

        private Code(int value) {
            this.value = value;
        }

        private static SparseArray<FirebaseFunctionsException.Code> buildStatusList() {
            SparseArray<FirebaseFunctionsException.Code> codes = new SparseArray();
            FirebaseFunctionsException.Code[] var1 = values();
            int var2 = var1.length;

            for(int var3 = 0; var3 < var2; ++var3) {
                FirebaseFunctionsException.Code c = var1[var3];
                FirebaseFunctionsException.Code existingValue = (FirebaseFunctionsException.Code)codes.get(c.ordinal());
                if (existingValue != null) {
                    throw new IllegalStateException("Code value duplication between " + existingValue + "&" + c.name());
                }

                codes.put(c.ordinal(), c);
            }

            return codes;
        }

        static FirebaseFunctionsException.Code fromValue(int value) {
            return (FirebaseFunctionsException.Code)STATUS_LIST.get(value, UNKNOWN);
        }

        static FirebaseFunctionsException.Code fromHttpStatus(int status) {
            switch(status) {
                case 200:
                    return OK;
                case 400:
                    return INVALID_ARGUMENT;
                case 401:
                    return UNAUTHENTICATED;
                case 403:
                    return PERMISSION_DENIED;
                case 404:
                    return NOT_FOUND;
                case 409:
                    return ABORTED;
                case 429:
                    return RESOURCE_EXHAUSTED;
                case 499:
                    return CANCELLED;
                case 500:
                    return INTERNAL;
                case 501:
                    return UNIMPLEMENTED;
                case 503:
                    return UNAVAILABLE;
                case 504:
                    return DEADLINE_EXCEEDED;
                default:
                    return UNKNOWN;
            }
        }
    }
}

