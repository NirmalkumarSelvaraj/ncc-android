package com.agero.ncc.db.database;


import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import com.agero.ncc.db.dao.AlertDao;
import com.agero.ncc.db.entity.Alert;

@Database(entities = Alert.class,version = 3, exportSchema = false)
@TypeConverters(DateConverter.class)
public abstract class NccDatabase extends RoomDatabase{

    public abstract AlertDao getAlertDao();

}
