package com.agero.ncc.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

public class TimerProgressBar extends View
{
    int progressColor=Color.BLUE;
    // % value of the progressbar.
    double progressBarValue = 0;
    double maxValue = 0;

    public TimerProgressBar(Context context)
    {
        super(context);
    }

    public TimerProgressBar(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        //progressBarValue = attrs.getAttributeIntValue(null, "progressBarValue", 0);
        progressColor = attrs.getAttributeIntValue(null, "progressColor", 0);
        //maxValue = attrs.getAttributeIntValue(null, "maxValue", 0);
    }

    public void setValue(double value)
    {
        progressBarValue = value;
        invalidate();
    }
    public void setProgressColor(int value)
    {
        progressColor = value;
        invalidate();
    }

    public void setMaxValue(double maxValue) {
        this.maxValue = maxValue;
        invalidate();
    }

    protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);

        float cornerRadius = 30.0f;

        // Draw the background of the bar.
        Paint backgroundPaint = new Paint();
        backgroundPaint.setColor(Color.LTGRAY);
        backgroundPaint.setStyle(Paint.Style.FILL);
        backgroundPaint.setAntiAlias(true);

        RectF backgroundRect = new RectF(0, 0, canvas.getWidth(), canvas.getHeight());
        canvas.drawRoundRect(backgroundRect, cornerRadius, cornerRadius, backgroundPaint);

        // Draw the progress bar.
        Paint barPaint = new Paint();
        barPaint.setColor(progressColor);
        barPaint.setStyle(Paint.Style.FILL);
        barPaint.setAntiAlias(true);

        float progress = (float) ((backgroundRect.width() / maxValue) * progressBarValue);
        RectF barRect = new RectF(0, 0, progress, canvas.getClipBounds().bottom);

        canvas.drawRoundRect(barRect, cornerRadius, cornerRadius, barPaint);

        //// Draw progress text in the middle.
        //Paint textPaint = new Paint();
        //textPaint.setColor(Color.WHITE);
        //textPaint.setTextSize(34);
        //
        //String text = progressBarValue + "%";
        //Rect textBounds = new Rect();
        //
        //textPaint.getTextBounds(text, 0, text.length(), textBounds);
        //
        //canvas.drawText(text,
        //        backgroundRect.centerX() - (textBounds.width() / 2),
        //        backgroundRect.centerY() + (textBounds.height() / 2),
        //        textPaint);

    }
}