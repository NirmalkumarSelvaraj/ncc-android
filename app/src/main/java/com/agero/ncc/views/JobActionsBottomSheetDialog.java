package com.agero.ncc.views;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.agero.ncc.BuildConfig;
import com.agero.ncc.R;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.utils.NccConstants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class JobActionsBottomSheetDialog extends BottomSheetDialogFragment {

    HomeActivity mHomeActivity;
    DispatcherJobOptionsDialogListener dispatcherJobOptionsDialogListener;
    @BindView(R.id.text_dispatcher_job_options)
    TextView mBottomSheetHeader;
    @BindView(R.id.image_modify_status)
    ImageView mImageModifyStaus;
    @BindView(R.id.text_modify_status)
    TextView mTextModifyStatus;
    @BindView(R.id.image_edit_service)
    ImageView mImageEditService;
    @BindView(R.id.text_edit_service)
    TextView mTextEditService;
    @BindView(R.id.image_tow_destination)
    ImageView mImageTowDestination;
    @BindView(R.id.text_tow_destination)
    TextView mTextTowDestination;
    @BindView(R.id.image_disablement_location)
    ImageView mImageDisablementLocation;
    @BindView(R.id.text_disablement_location)
    TextView mTextDisablementLocation;
    @BindView(R.id.image_update_eta)
    ImageView mImageUpdateEta;
    @BindView(R.id.text_update_eta)
    TextView mTextUpdateEta;

    @BindView(R.id.image_assign_driver)
    ImageView mImageAssignDriver;
    @BindView(R.id.text_assign_driver)
    TextView mTextAssignDriver;
    @BindView(R.id.image_report_goa)
    ImageView mImageReportGoa;
    @BindView(R.id.text_report_goa)
    TextView mTextReportGoa;
    @BindView(R.id.image_cancel_service)
    ImageView mImageCancelService;
    @BindView(R.id.text_cancel_service)
    TextView mTextCancelService;
    @BindView(R.id.text_geofence_jobs)
    TextView mTextGeofence;
    @BindView(R.id.image_geofence_jobs)
    ImageView mImageGeofence;

    boolean isTow;
    boolean isUpdateEtaPossible;
    private String statusCode;


    public static JobActionsBottomSheetDialog getInstance(boolean isTow, boolean isUpdateEta, String statusCode) {
        JobActionsBottomSheetDialog fragment = new JobActionsBottomSheetDialog();
        Bundle args = new Bundle();
        args.putBoolean(NccConstants.IS_TOW, isTow);
        args.putBoolean(NccConstants.UPDATE_ETA, isUpdateEta);
        args.putString(NccConstants.JOB_STATUS, statusCode);
        fragment.setArguments(args);
        return fragment;
    }

    public void setDispatcherJobOptionsDialogListener(DispatcherJobOptionsDialogListener dispatcherJobOptionsDialogListener) {
        this.dispatcherJobOptionsDialogListener = dispatcherJobOptionsDialogListener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.job_bottom_sheet, container, false);
        ButterKnife.bind(this, view);
        mHomeActivity = (HomeActivity) getActivity();
        if (getArguments() != null) {
            isTow = getArguments().getBoolean(NccConstants.IS_TOW);
            isUpdateEtaPossible = getArguments().getBoolean(NccConstants.UPDATE_ETA);
            statusCode = getArguments().getString(NccConstants.JOB_STATUS);
        }

        boolean driver = mHomeActivity.isLoggedInDriver();

        if(driver){
            mBottomSheetHeader.setText(getString(R.string.label_driver_job_options));
        }else{
            mBottomSheetHeader.setText(getString(R.string.label_dispatcher_job_options));
        }

        mImageTowDestination.setVisibility(isTow ? View.VISIBLE : View.GONE);
        mTextTowDestination.setVisibility(isTow ? View.VISIBLE : View.GONE);
        mImageGeofence.setVisibility(View.GONE);
        mTextGeofence.setVisibility(View.GONE);
        if((driver || mHomeActivity.isUserDispatcherAndDriver()) && BuildConfig.FLAVOR != "prod") {
                mImageGeofence.setVisibility(View.VISIBLE);
                mTextGeofence.setVisibility(View.VISIBLE);
        }
        mImageAssignDriver.setVisibility(driver ? View.GONE : View.VISIBLE);
        mTextAssignDriver.setVisibility(driver ? View.GONE : View.VISIBLE);
        mImageReportGoa.setVisibility(driver ? View.GONE : View.VISIBLE);
        mTextReportGoa.setVisibility(driver ? View.GONE : View.VISIBLE);
        mImageCancelService.setVisibility(driver ? View.GONE : View.VISIBLE);
        mTextCancelService.setVisibility(driver ? View.GONE : View.VISIBLE);

        mImageUpdateEta.setVisibility(isUpdateEtaPossible ? View.VISIBLE : View.GONE);
        mTextUpdateEta.setVisibility(isUpdateEtaPossible ? View.VISIBLE : View.GONE);
        mTextModifyStatus.setVisibility(NccConstants.JOB_STATUS_UNASSIGNED.equalsIgnoreCase(statusCode) ? View.GONE : View.VISIBLE);
        mImageModifyStaus.setVisibility(NccConstants.JOB_STATUS_UNASSIGNED.equalsIgnoreCase(statusCode) ? View.GONE : View.VISIBLE);

        //disable DL,TL update
        mImageDisablementLocation.setVisibility(View.GONE);
        mTextDisablementLocation.setVisibility(View.GONE);
        mImageTowDestination.setVisibility(View.GONE);
        mTextTowDestination.setVisibility(View.GONE);


        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                BottomSheetDialog dialog = (BottomSheetDialog) getDialog();
                FrameLayout bottomSheet = (FrameLayout)
                        dialog.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
                behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                behavior.setPeekHeight(0);
            }
        });
    }

    String eventAction = "";
    String eventCategory =NccConstants.FirebaseEventCategory.JOB_DETAILS;
    @OnClick({R.id.image_geofence_jobs, R.id.text_geofence_jobs,R.id.image_modify_status, R.id.text_modify_status, R.id.image_update_eta, R.id.text_update_eta, R.id.image_assign_driver, R.id.text_assign_driver, R.id.image_report_goa, R.id.text_report_goa, R.id.image_service_unsuccessful, R.id.text_service_unsuccessful, R.id.image_cancel_service, R.id.text_cancel_service, R.id.image_edit_service, R.id.text_edit_service, R.id.image_tow_destination, R.id.text_tow_destination, R.id.image_disablement_location, R.id.text_disablement_location})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_geofence_jobs:
            case R.id.text_geofence_jobs:
                dispatcherJobOptionsDialogListener.onGeofenceJobsClick();
                break;
            case R.id.image_modify_status:
            case R.id.text_modify_status:
                eventAction = NccConstants.FirebaseEventAction.AS_MODIFY_STATUS;
                dispatcherJobOptionsDialogListener.onModifyStatusClick();
                break;
            case R.id.image_update_eta:
            case R.id.text_update_eta:
                eventAction = NccConstants.FirebaseEventAction.AS_UPDATE_ETA;
                dispatcherJobOptionsDialogListener.onUpdateEtaClick();
                break;
            case R.id.image_assign_driver:
            case R.id.text_assign_driver:
                eventAction = NccConstants.FirebaseEventAction.AS_ASSIGN_DRIVER;
                dispatcherJobOptionsDialogListener.onAssignDriverClick();
                break;
            case R.id.image_report_goa:
            case R.id.text_report_goa:
                eventAction = NccConstants.FirebaseEventAction.AS_REPORT_GOA;
                dispatcherJobOptionsDialogListener.onReportGoaClick();
                break;
            case R.id.image_service_unsuccessful:
            case R.id.text_service_unsuccessful:
                eventAction = NccConstants.FirebaseEventAction.AS_SERVICE_UNSUCCESSFUL;
                dispatcherJobOptionsDialogListener.onServiceUnsuccessClick();
                break;
            case R.id.image_cancel_service:
            case R.id.text_cancel_service:
                eventAction = NccConstants.FirebaseEventAction.AS_CANCEL_SERVICE;
                dispatcherJobOptionsDialogListener.onCancelServiceClick();
                break;
            case R.id.image_edit_service:
            case R.id.text_edit_service:
                eventAction = NccConstants.FirebaseEventAction.AS_EDIT_SERVICE;
                dispatcherJobOptionsDialogListener.onEditServiceClick();
                break;
            case R.id.image_tow_destination:
            case R.id.text_tow_destination:
                eventAction = NccConstants.FirebaseEventAction.AS_CHANGE_TOW_DESTINATION;
                dispatcherJobOptionsDialogListener.onEditTowDestination();
                break;
            case R.id.image_disablement_location:
            case R.id.text_disablement_location:
                eventAction = NccConstants.FirebaseEventAction.AS_CHANGE_DISABLEMENT;
                dispatcherJobOptionsDialogListener.onEditDisablementLocation();
                break;
        }
        mHomeActivity.firebaseLogEvent(eventCategory,eventAction);
    }


    public interface DispatcherJobOptionsDialogListener {

        void onGeofenceJobsClick();

        void onModifyStatusClick();

        void onUpdateEtaClick();

        void onAssignDriverClick();

        void onReportGoaClick();

        void onServiceUnsuccessClick();

        void onCancelServiceClick();

        void onEditServiceClick();

        void onEditTowDestination();

        void onEditDisablementLocation();

    }
}
